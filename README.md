# SmartGarageGoodOldBilly

NOTE: Development is in progress.

Car Service Management System. This is a web application that enables the owners of an auto repair shop to manage their
day-to-day job and gives customers option to retrieve details for each shop visit.

Following is the database the project is working with.

Database is hosted on AWS.

Database is migrated by Flyway.

![img.png](img.png)

# REST API

The REST API provides the following capabilities:

1. Users · CRUD Operations · Employees are able to search a user by username, email, phone, or car’s license plate/VIN
   Users receive a confirmation e-mail upon registration. Their account gets enabled after clicking the confirmation
   link.
2. Vehicles · CRUD Operations · Search a vehicle by license plate or VIN · Employee must be able to search vehicles by
   owner’s phone number · Filter vehicles by model, brand, year of creation · Set user functionality.
3. Services · CRUD Operations · Search by name · Search by price · Filter and sort by name and price range
4. Brands · CRUD Operations
5. Models · CRUD Operations
6. Roles · CRUD Operations
7. Visits · CRUD Operations · Set user functionality. · Add service functionality.

## Documentation

Swagger documentation available on http://localhost:8080/swagger-ui/

## Authors and acknowledgment

Third-party services used:
Currency converter API by Dimitri Posadskiy, https://github.com/posadskiy/currency-converter.

## License

Telerik Academy.