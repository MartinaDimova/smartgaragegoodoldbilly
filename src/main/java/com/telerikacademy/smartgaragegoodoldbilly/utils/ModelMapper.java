package com.telerikacademy.smartgaragegoodoldbilly.utils;

import com.telerikacademy.smartgaragegoodoldbilly.models.*;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.*;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.*;
import org.springframework.stereotype.Component;

@Component
public class ModelMapper {

    private final RoleRepository roleRepository;
    private final BrandRepository brandRepository;
    private final ModelRepository modelRepository;
    private final VehicleRepository vehicleRepository;
    private final UserRepository userRepository;


    public ModelMapper(RoleRepository roleRepository, BrandRepository brandRepository, ModelRepository modelRepository,
                       VehicleRepository vehicleRepository, UserRepository userRepository) {
        this.roleRepository = roleRepository;
        this.brandRepository = brandRepository;
        this.modelRepository = modelRepository;
        this.vehicleRepository = vehicleRepository;
        this.userRepository = userRepository;
    }

    public User fromDto(UserDto userDtoIn) {
        Role role = roleRepository.getById(userDtoIn.getRoleId());
        User user = new User();
        user.setUsername(userDtoIn.getUsername());
        user.setPassword(userDtoIn.getPassword());
        user.setEmail(userDtoIn.getEmail());
        user.setPhone_number(userDtoIn.getPhone_number());
        user.addUserRole(role);
        return user;
    }

    public Model fromDto(ModelDto modelDto) {
        Brand brand = brandRepository.getById(modelDto.getBrand_id());
        Model model = new Model();
        model.setModel(modelDto.getModel());
        model.setBrand(brand);
        return model;
    }

    public Vehicle fromDto(VehicleDto vehicleDto) {
        Model model = modelRepository.getById(vehicleDto.getModel_id());
        User user = userRepository.getById(vehicleDto.getUser_id());
        Vehicle vehicle = new Vehicle();
        vehicle.setLicense_plate(vehicleDto.getLicense_plate());
        vehicle.setVin(vehicleDto.getVin());
        vehicle.setYear(vehicleDto.getYear());
        vehicle.setModel(model);
        vehicle.setUser(user);
        return vehicle;
    }

    public Visit fromDto(VisitDto visitDto) {
        Vehicle vehicle = vehicleRepository.getById(visitDto.getVehicle_id());
        Visit visit = new Visit();
        visit.setDate(visitDto.getVisit_date());
        visit.setVehicle(vehicle);
        visit.setUser(vehicle.getUser());
        return visit;
    }

    public Visit updateFromDto(VisitDto visitDto, Visit visit) {
        Vehicle vehicle = vehicleRepository.getById(visitDto.getVehicle_id());
        visit.setDate(visitDto.getVisit_date());
        visit.setVehicle(vehicle);
        return visit;
    }

    public VisitDtoOut transformVisit(Visit visit, VisitDtoOut visitDtoOut) {
        visitDtoOut.setId(visit.getId());
        visitDtoOut.setDate(visit.getDate());
        visitDtoOut.setTotal_cost(visit.getTotal_cost());
        visitDtoOut.setUser(visit.getUser());
        visitDtoOut.setVehicle(visit.getVehicle());
        visitDtoOut.setHVisitServices(visit.getHVisitServices());
        return visitDtoOut;
    }
}