package com.telerikacademy.smartgaragegoodoldbilly.controllers.rest;

import com.telerikacademy.smartgaragegoodoldbilly.models.User;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.UserDto;
import com.telerikacademy.smartgaragegoodoldbilly.services.RegistrationService;
import com.telerikacademy.smartgaragegoodoldbilly.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "api/registration")
public class RegistrationController {

    private final RegistrationService registrationService;
    private final ModelMapper modelMapper;

    @Autowired
    public RegistrationController(RegistrationService registrationService, ModelMapper modelMapper) {
        this.registrationService = registrationService;
        this.modelMapper = modelMapper;
    }

    @PostMapping
    public String register(@Valid @RequestBody UserDto userDto) {
        User user = modelMapper.fromDto(userDto);
        return registrationService.register(user);
    }

    @GetMapping(path = "confirm")
    public String confirm(@RequestParam("token") String token) {
        return registrationService.confirmToken(token);
    }

    @PutMapping(path = "/forgotten-password")
    public String resetPassword(@RequestParam(required = false) String email) {
        return registrationService.resetPassword(email);
    }
}
