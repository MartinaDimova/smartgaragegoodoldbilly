package com.telerikacademy.smartgaragegoodoldbilly.controllers.rest;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.models.Vehicle;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.VehicleDto;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.VehicleService;
import com.telerikacademy.smartgaragegoodoldbilly.utils.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/vehicles")
public class VehicleController {

    private final VehicleService vehicleService;
    private final ModelMapper modelMapper;

    public VehicleController(VehicleService vehicleService, ModelMapper modelMapper) {
        this.vehicleService = vehicleService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/search")
    public List<Vehicle> searchByLicensePlateVinTelNumber(@RequestParam(required = false) Optional<String> search) {
        try {
            return vehicleService.searchByLicensePlateVinTelNumber(search);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filterByBrand")
    public List<Vehicle> filterBrand(@RequestParam(required = false) Optional<String> brand) {
        try {
            return vehicleService.filterByBrand(brand);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filterByModel")
    public List<Vehicle> filterByModel(@RequestParam(required = false) Optional<String> model) {
        try {
            return vehicleService.filterByModel(model);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filterByYear")
    public List<Vehicle> filterByYear(@RequestParam(required = false) Optional<String> year) {
        try {
            return vehicleService.filterByYear(year);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public List<Vehicle> getAll() {
        return vehicleService.getAll();
    }

    @GetMapping("/id/{id}")
    public Vehicle getById(@PathVariable int id) {
        try {
            return vehicleService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/license_plate/{license_plate}")
    public Vehicle getByLicense_Plate(@PathVariable String license_plate) {
        try {
            return vehicleService.getByLicensePlate(license_plate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Vehicle create(@Valid @RequestBody VehicleDto vehicleDto) {
        try {
            Vehicle vehicle = modelMapper.fromDto(vehicleDto);
            return vehicleService.create(vehicle);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public Vehicle update(@Valid @RequestBody Vehicle vehicle) {
        try {
            return vehicleService.update(vehicle);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            vehicleService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotAllowedOperation e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage());
        }
    }

    @PutMapping("/{vehicle_id}/setuser/{user_id}")
    public void setUser(@PathVariable int vehicle_id, @PathVariable int user_id) {
        try {
            vehicleService.setUser(vehicle_id, user_id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}