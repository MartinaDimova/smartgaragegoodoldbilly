package com.telerikacademy.smartgaragegoodoldbilly.controllers.rest;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.models.Service;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.ServicePriceRangeOptions;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.ServiceSortOptions;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.ServiceService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/services")
public class ServiceController {

    private final ServiceService serviceService;

    public ServiceController(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    @GetMapping("/search")
    public List<Service> search(@RequestParam(required = false) Optional<String> search) {
            return serviceService.search(search);
    }

    @GetMapping("/filter")
    public List<Service> filter(@RequestParam(required = false) Optional<ServicePriceRangeOptions> price,
                                @RequestParam(required = false) Optional<ServiceSortOptions> sortOptions) {
        try {
            return serviceService.filter(price, sortOptions);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public List<Service> getAll() {
        return serviceService.getAll();
    }

    @GetMapping("/id/{id}")
    public Service getById(@PathVariable int id) {
        try {
            return serviceService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/service/{service}")
    public Service getByService(@PathVariable String service) {
        try {
            return serviceService.getByService(service);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Service create(@Valid @RequestBody Service service) {
        try {
            return serviceService.create(service);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public Service update(@Valid @RequestBody Service service) {
        try {
            return serviceService.update(service);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            serviceService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotAllowedOperation e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage());
        }
    }
}