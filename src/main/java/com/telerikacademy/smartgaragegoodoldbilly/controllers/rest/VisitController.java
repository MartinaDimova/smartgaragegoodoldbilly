package com.telerikacademy.smartgaragegoodoldbilly.controllers.rest;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.models.Visit;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.VisitDto;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.VisitService;
import com.telerikacademy.smartgaragegoodoldbilly.utils.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/visits")
public class VisitController {

    private final VisitService visitService;
    private final ModelMapper modelMapper;

    public VisitController(VisitService visitService, ModelMapper modelMapper) {
        this.visitService = visitService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<Visit> getAll() {
        return visitService.getAll();
    }

    @GetMapping("/{id}")
    public Visit getById(@PathVariable int id) {
        try {
            return visitService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Visit create(@Valid @RequestBody VisitDto visitDto) {
        Visit visit = modelMapper.fromDto(visitDto);
        return visitService.create(visit);
    }

    @PutMapping
    public Visit update(@Valid @RequestBody Visit visit) {
        try {
            return visitService.update(visit);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            visitService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotAllowedOperation e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage());
        }
    }

    @PutMapping("/{visit_id}/setuser")
    public void setUser(@PathVariable int visit_id) {
        try {
            visitService.setUser(visit_id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{visit_id}/addService")
    public void addService(@PathVariable int visit_id, @PathVariable int service_id) {
        try {
            visitService.addService(visit_id, Optional.of(service_id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{visit_id}/removeService")
    public void removeService(@PathVariable int visit_id, @PathVariable int service_id) {
        try {
            visitService.removeService(visit_id, Optional.of(service_id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
