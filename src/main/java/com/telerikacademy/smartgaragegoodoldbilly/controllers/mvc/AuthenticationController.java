package com.telerikacademy.smartgaragegoodoldbilly.controllers.mvc;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.models.Brand;
import com.telerikacademy.smartgaragegoodoldbilly.models.Service;
import com.telerikacademy.smartgaragegoodoldbilly.models.Vehicle;
import com.telerikacademy.smartgaragegoodoldbilly.models.Visit;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.EmailDto;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.LoginDto;
import com.telerikacademy.smartgaragegoodoldbilly.services.RegistrationService;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.BrandService;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.VehicleService;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping()
public class AuthenticationController {

    private final RegistrationService registrationService;
    private final VehicleService vehicleService;
    private final VisitService visitService;
    private final BrandService brandService;

    @Autowired
    public AuthenticationController(RegistrationService registrationService, VehicleService vehicleService, VisitService visitService, BrandService brandService) {
        this.registrationService = registrationService;
        this.vehicleService = vehicleService;
        this.visitService = visitService;
        this.brandService = brandService;
    }

    @GetMapping("/")
    public String showLoginPage(Model model) {
        LoginDto loginDto = new LoginDto();
        model.addAttribute("login", loginDto);
        return "index";
    }

    @RequestMapping("/services")
    public ModelAndView employeeDashboard() {
        return new ModelAndView("services");
    }

    @RequestMapping("/index-customer")
    public ModelAndView userDashboard(Model model, Principal principal, @ModelAttribute("service") Service service, Visit visit,
                                      @ModelAttribute("brand") Brand brand,
                                      @ModelAttribute("model")com.telerikacademy.smartgaragegoodoldbilly.models.Model vehicleModel) {
        List<Vehicle> userVehicles = vehicleService.filterByUsername(Optional.ofNullable(principal.getName()));
        List<Visit> userVisits = visitService.getAllForCustomers();
        List<Brand> allBrands = brandService.getAll();
        model.addAttribute("visit", visit);
        model.addAttribute("allBrands", allBrands);
        model.addAttribute("userVehicles", userVehicles);
        model.addAttribute("userVisits", userVisits);
        Vehicle vehicle = new Vehicle();
        model.addAttribute("vehicle", vehicle);
        return new ModelAndView("index-customer");
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/forgotten-password")
    public String displayResetPassword(Model model) {
        model.addAttribute("email", new EmailDto());
        return "forgotten-password";
    }

    @PostMapping("/forgotten-password")
    public String forgotUserPassword(Model model, @Valid @ModelAttribute("email") EmailDto emailDto, BindingResult errors) {
        if (errors.hasErrors()) {
            return "forgotten-password";
        }
        try {
            model.addAttribute("email", emailDto);
            registrationService.resetPassword(emailDto.getEmail());
            return "index";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}