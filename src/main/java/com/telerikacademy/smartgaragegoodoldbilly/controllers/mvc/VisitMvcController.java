package com.telerikacademy.smartgaragegoodoldbilly.controllers.mvc;

import com.posadskiy.currencyconverter.enums.Currency;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.*;
import com.telerikacademy.smartgaragegoodoldbilly.models.Service;
import com.telerikacademy.smartgaragegoodoldbilly.models.User;
import com.telerikacademy.smartgaragegoodoldbilly.models.Vehicle;
import com.telerikacademy.smartgaragegoodoldbilly.models.Visit;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.VisitDto;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.VisitDtoOut;
import com.telerikacademy.smartgaragegoodoldbilly.services.PdfGenerateService;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.ServiceService;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.UserService;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.VehicleService;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.VisitService;
import com.telerikacademy.smartgaragegoodoldbilly.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/visits")
public class VisitMvcController {

    private final VisitService visitService;
    private final ModelMapper modelMapper;
    private final UserService userService;
    private final VehicleService vehicleService;
    private final ServiceService serviceService;
    private final PdfGenerateService pdfGenerateService;

    @Autowired
    public VisitMvcController(VisitService visitService, ModelMapper modelMapper, UserService userService, VehicleService vehicleService, ServiceService serviceService, PdfGenerateService pdfGenerateService) {
        this.visitService = visitService;
        this.modelMapper = modelMapper;
        this.userService = userService;
        this.vehicleService = vehicleService;
        this.serviceService = serviceService;
        this.pdfGenerateService = pdfGenerateService;
    }

    @ModelAttribute("users")
    public List<User> populateUsers() {
        return userService.getAll();
    }

    @ModelAttribute("vehicles")
    public List<Vehicle> populateVehicles() {
        return vehicleService.getAll();
    }

    @ModelAttribute("services")
    public List<Service> populateServices() {
        return serviceService.getAll();
    }

    @ModelAttribute("service")
    public Service populateService() {
        return new Service();
    }

    @ModelAttribute("visitDto")
    public VisitDto populateVisitDto() {
        return new VisitDto();
    }

    @ModelAttribute("visitDtoOut")
    public VisitDtoOut populateVisitDtoOut() {
        return new VisitDtoOut();
    }

    @ModelAttribute("currency")
    public Currency[] populateCurrency() {
        return Currency.values();
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/filter")
    public String filter(Model model,
                         @RequestParam(value = "date", required = false) Optional<Date> date) {
        model.addAttribute("visits", visitService.filter(date));
        return "visits";
    }

    @GetMapping("/userFilter")
    public String userFilter(Model model,
                             @RequestParam(value = "date", required = false) Optional<Date> date) {
        model.addAttribute("visits", visitService.filter(date));
        return "index-customer";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping
    public String showAllVisits(Model model) {
        model.addAttribute("visits", visitService.getAll());
        return "visits";
    }

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/customers")
    public String showAllVisitsForCustomers(Model model) {
        model.addAttribute("visits", visitService.getAllForCustomers());
        return "visits";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id/id}")
    public String showSingleVisit(Model model, @PathVariable int id) {
        try {
            model.addAttribute("visit", visitService.getById(id));
            return "visits";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{date/date}")
    public String showSingleVisit(Model model, @PathVariable Date date) {
        try {
            model.addAttribute("visit", visitService.getByDate(date));
            return "visits";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/new")
    public String showNewVisitPage(Model model) {
        model.addAttribute("visit", new VisitDto());
        return "redirect:/visits";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/new")
    public String createVisit(@Valid @ModelAttribute("visitDto") VisitDto visitDto, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> model.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            Visit visit = modelMapper.fromDto(visitDto);
            visitService.create(visit);
            return "redirect:/visits";
        } catch (DuplicateEntityException | WrongUserInput e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/update")
    public String showEditVisitPage(Model model, @PathVariable int id) {
        try {
            Visit visit = visitService.getById(id);
            model.addAttribute("visitId", id);
            model.addAttribute("visit", visit);
            return "redirect:/visits";
        } catch (EntityNotFoundException | WrongUserInput e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/{id}/update")
    public String updateVisit(Model model, @PathVariable int id,
                              @Valid @ModelAttribute("visitDto") VisitDto visitDto, BindingResult errors) {
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> model.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            Visit visit = visitService.getById(id);
            Visit updatedVisit = modelMapper.updateFromDto(visitDto, visit);
            visitService.update(updatedVisit);
            return "redirect:/visits";
        } catch (EntityNotFoundException | WrongUserInput e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/delete")
    public String deleteVisit(Model model, @PathVariable int id) {
        try {
            visitService.delete(id);
            return "redirect:/visits";
        } catch (EntityNotFoundException | NotAllowedOperation e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/{id}/setUser")
    public String setUser(Model model, @PathVariable int id) {
        try {
            visitService.setUser(id);
            model.addAttribute("visit", visitService.getById(id));
            return "redirect:/visits";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{visit_id}/addService")
    public String addService(Model model, @PathVariable int visit_id,
                             @RequestParam(value = "service_id", required = false) Optional<Integer> service_id) {
        try {
            visitService.addService(visit_id, service_id);
            return "redirect:/visits";
        } catch (EntityNotFoundException | NotAllowedOperation | NullPointerException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{visit_id}/removeService")
    public String removeService(Model model, @PathVariable int visit_id,
                                @RequestParam(value = "service_id", required = false) Optional<Integer> service_id) {
        try {
            visitService.removeService(visit_id, service_id);
            return "redirect:/visits";
        } catch (EntityNotFoundException | NotAllowedOperation | NullPointerException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/convertTotalCost")
    public String convertTotalCost(Model model, @PathVariable int id, @ModelAttribute VisitDtoOut visitDtoOut,
                                   @ModelAttribute Service service,
                                   @RequestParam(value = "currency", required = false) Currency currency) {
        try {
            Visit visit = visitService.getById(id);
            visitDtoOut = modelMapper.transformVisit(visit, visitDtoOut);
            visitDtoOut = visitService.makeReportNewCurrency(visitDtoOut, currency);
            model.addAttribute("visit", visitDtoOut);
            model.addAttribute("visit-pdf", visitDtoOut);
            String pdfFileName = "\\C:\\Users\\Martina Dimova\\pdf\\ReportVisit.pdf";
            pdfGenerateService.generatePdfFile("visit-pdf", visitDtoOut, pdfFileName);
            return "visit";
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}