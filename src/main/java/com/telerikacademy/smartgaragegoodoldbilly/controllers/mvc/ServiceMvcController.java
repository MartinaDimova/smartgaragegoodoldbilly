package com.telerikacademy.smartgaragegoodoldbilly.controllers.mvc;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.WrongUserInput;
import com.telerikacademy.smartgaragegoodoldbilly.models.Service;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.FilterServiceDto;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.ServicePriceRangeOptions;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.ServiceSortOptions;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/services")
public class ServiceMvcController {

    private final ServiceService serviceService;

    @Autowired
    public ServiceMvcController(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    @ModelAttribute("service")
    public Service populateService() {
        return new Service();
    }

    @ModelAttribute("filterServiceDto")
    public FilterServiceDto populateFilterServiceDto() {
        return new FilterServiceDto();
    }

    @ModelAttribute("priceRangeOptions")
    public ServicePriceRangeOptions[] populatePriceRangeOptions() {
        return ServicePriceRangeOptions.values();
    }

    @ModelAttribute("sortOptions")
    public ServiceSortOptions[] populateSortOptions() {
        return ServiceSortOptions.values();
    }

    @GetMapping("/search")
    public String searchServices(Model model, @RequestParam(value = "search", required = false)
            Optional<String> search) {
        model.addAttribute("services", serviceService.search(search));
        return "services";
    }

    @PostMapping("/filter")
    public String filterServices(@ModelAttribute("filterServiceDto") FilterServiceDto filterServiceDto, Model model) {
        var filtered = serviceService.filter(
                Optional.ofNullable(filterServiceDto.getPrice() == null ? null : ServicePriceRangeOptions.valueOfPreview(filterServiceDto.getPrice())),
                Optional.ofNullable(filterServiceDto.getSortOptions() == null ? null : ServiceSortOptions.valueOfPreview(filterServiceDto.getSortOptions()))
        );
        model.addAttribute("filterServiceDto", new FilterServiceDto());
        model.addAttribute("services", filtered);
        return "services";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping
    public String showAllServices(Model model) {
        model.addAttribute("services", serviceService.getAll());
        return "services";
    }

    @GetMapping("/id/{id}")
    public String showSingleService(Model model, @PathVariable int id) {
        try {
            model.addAttribute("service", serviceService.getById(id));
            return "services";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/service/{service}")
    public String showSingleService(Model model, @PathVariable String service) {
        try {
            model.addAttribute("service", serviceService.getByService(service));
            return "services";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/new")
    public String showNewServicePage(Model model) {
        model.addAttribute("service", new Service());
        return "redirect:/services";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/new")
    public String createService(@Valid @ModelAttribute("service") Service service, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> model.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            serviceService.create(service);
            return "redirect:/services";
        } catch (DuplicateEntityException | EntityNotFoundException | WrongUserInput e) {
            errors.rejectValue("service", "duplicate_service", e.getMessage());
            return "redirect:/services";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/update")
    public String showEditServicePage(Model model, @PathVariable int id) {
        try {
            Service service = serviceService.getById(id);
            model.addAttribute("serviceId", id);
            model.addAttribute("service", service);
            return "redirect:/services";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/{id}/update")
    public String updateService(Model model, @Valid @ModelAttribute("service") Service service, BindingResult errors) {
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> model.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            serviceService.update(service);
            return "redirect:/services";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/delete")
    public String deleteService(Model model, @PathVariable int id, @ModelAttribute("service") Service service) {
        try {
            model.addAttribute("service", service);
            serviceService.delete(id);
            return "redirect:/services";
        } catch (EntityNotFoundException | NotAllowedOperation e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}