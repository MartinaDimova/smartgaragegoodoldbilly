package com.telerikacademy.smartgaragegoodoldbilly.controllers.mvc;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.WrongUserInput;
import com.telerikacademy.smartgaragegoodoldbilly.models.*;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.FilterUserDto;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.UserDto;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.UserSortOptions;
import com.telerikacademy.smartgaragegoodoldbilly.services.RegistrationService;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.*;
import com.telerikacademy.smartgaragegoodoldbilly.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final RoleService roleService;
    private final ModelMapper modelMapper;
    private final RegistrationService registrationService;
    private final VehicleService vehicleService;
    private final BrandService brandService;
    private final ModelService modelService;
    private final VisitService visitService;

    @Autowired
    public UserMvcController(UserService userService, RoleService roleService, ModelMapper modelMapper, RegistrationService registrationService, VehicleService vehicleService, BrandService brandService, ModelService modelService, VisitService visitService) {
        this.userService = userService;
        this.roleService = roleService;
        this.modelMapper = modelMapper;
        this.registrationService = registrationService;
        this.vehicleService = vehicleService;
        this.brandService = brandService;
        this.modelService = modelService;
        this.visitService = visitService;
    }

    @ModelAttribute("roles")
    public List<Role> populateRoles() {
        return roleService.getAll();
    }

    @ModelAttribute("vehicles")
    public List<Vehicle> populateVehicles() {
        return vehicleService.getAll();
    }

    @ModelAttribute("brands")
    public List<Brand> populateBrands() {
        return brandService.getAll();
    }

    @ModelAttribute("models")
    public List<com.telerikacademy.smartgaragegoodoldbilly.models.Model> populateModels() {
        return modelService.getAll();
    }

    @ModelAttribute("visits")
    public List<Visit> populateVisits() {
        return visitService.getAll();
    }

    @ModelAttribute("filterUserDto")
    public FilterUserDto populateFilterUserDto() {
        return new FilterUserDto();
    }

    @ModelAttribute("userDto")
    public UserDto populateUserDto() {
        return new UserDto();
    }

    @ModelAttribute("sortOptions")
    public UserSortOptions[] populateSortOptions() {
        return UserSortOptions.values();
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/search")
    public String searchUsers(Model model, @RequestParam(value = "search", required = false)
            Optional<String> search) {
        model.addAttribute("users", userService.search(search));
        return "users";
    }


    @PostMapping("/filter")
    public String filterUsers(@ModelAttribute("filterUserDto") FilterUserDto filterUserDto,
                              Model model) {
        var filtered = userService.filter(
                Optional.ofNullable(filterUserDto.getDateFrom() == null ? null : filterUserDto.getDateFrom()),
                Optional.ofNullable(filterUserDto.getDateTo() == null ? null : filterUserDto.getDateTo()),
                Optional.ofNullable(filterUserDto.getSortOptions() == null ? null : UserSortOptions.valueOfPreview(filterUserDto.getSortOptions()))
        );
        model.addAttribute("filterUserDto", new FilterUserDto());
        model.addAttribute("users", filtered);
        return "users";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping
    public String showAllUsers(Model model) {
        try {
            model.addAttribute("users", userService.getAll());
            return "users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/id/{id}")
    public String showSingleUser(Model model, @PathVariable int id) {
        try {
            model.addAttribute("user", userService.getById(id));
            return "users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/username/{username}")
    public String showSingleUser(Model model, @PathVariable String username) {
        try {
            model.addAttribute("user", userService.getByUsername(username));
            return "users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/new")
    public String showNewUserPage(Model model) {
        model.addAttribute("user", new UserDto());
        return "users";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/new")
    public String createUser(Model model, @Valid @ModelAttribute UserDto userDto, BindingResult errors) {
        User user;
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> model.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            user = modelMapper.fromDto(userDto);
            registrationService.register(user);
            return "users";
        } catch (DuplicateEntityException | EntityNotFoundException | WrongUserInput e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(Model model, @PathVariable int id, @ModelAttribute Role role) {
        try {
            User user = userService.getById(id);
            model.addAttribute("username", user.getUsername());
            model.addAttribute("user", user);
            model.addAttribute("role", role);
            return "users";
        } catch (EntityNotFoundException | NotAllowedOperation e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(Model model, @Valid @ModelAttribute("user") User user, BindingResult errors) {
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> model.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            userService.update(user);
            return "users";
        } catch (EntityNotFoundException | NotAllowedOperation e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/delete")
    public String deleteUser(Model model, @PathVariable int id, @ModelAttribute("user") User user) {
        try {
            model.addAttribute("user", user);
            userService.delete(id);
            return "users";
        } catch (EntityNotFoundException | NotAllowedOperation e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}