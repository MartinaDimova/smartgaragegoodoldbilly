package com.telerikacademy.smartgaragegoodoldbilly.controllers.mvc;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.models.Brand;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/brands")
public class BrandMvcController {

    private final BrandService brandService;

    @Autowired
    public BrandMvcController(BrandService brandService) {
        this.brandService = brandService;
    }

    @GetMapping
    public String showAllBrands(Model model) {
        model.addAttribute("brands", brandService.getAll());
        return "brands";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/id/{id}")
    public String showSingleBrand(Model model, @PathVariable int id) {
        try {
            model.addAttribute("brand", brandService.getById(id));
            return "brand";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/brand/{brand}")
    public String showSingleBrand(Model model, @PathVariable String brand) {
        try {
            model.addAttribute("brand", brandService.getByBrand(brand));
            return "brand";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/new")
    public String showNewBrandPage(Model model) {
        model.addAttribute("brand", new Brand());
        return "brand-new";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/new")
    public String createBrand(@Valid @ModelAttribute("brand") Brand brand, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> model.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            brandService.create(brand);
            return "redirect:/brands";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("brand", "duplicate_brand", e.getMessage());
            return "brand-new";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/update")
    public String showEditBrandPage(Model model, @PathVariable int id) {
        try {
            Brand brand = brandService.getById(id);
            model.addAttribute("brandId", id);
            model.addAttribute("brand", brand);
            return "brand-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/{id}/update")
    public String updateBrand(Model model,
                              @Valid @ModelAttribute("brand") Brand brand, BindingResult errors) {
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> model.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            brandService.update(brand);
            return "redirect:/brands";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/delete")
    public String deleteBrand(Model model, @PathVariable int id) {
        try {
            brandService.delete(id);
            return "redirect:/brands";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}