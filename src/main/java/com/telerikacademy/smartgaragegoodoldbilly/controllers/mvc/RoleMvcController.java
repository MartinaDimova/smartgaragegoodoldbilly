package com.telerikacademy.smartgaragegoodoldbilly.controllers.mvc;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.models.Role;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/roles")
public class RoleMvcController {

    private final RoleService roleService;

    @Autowired
    public RoleMvcController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping
    public String showAllRoles(Model model) {
        model.addAttribute("roles", roleService.getAll());
        return "roles";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/id/{id}")
    public String showSingleRole(Model model, @PathVariable int id) {
        try {
            model.addAttribute("role", roleService.getById(id));
            return "role";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/role/{role}")
    public String showSingleRole(Model model, @PathVariable String role) {
        try {
            model.addAttribute("role", roleService.getByRole(role));
            return "role";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/new")
    public String showNewRolePage(Model model) {
        model.addAttribute("role", new Role());
        return "role-new";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/new")
    public String createRole(@Valid @ModelAttribute("role") Role role, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> model.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            roleService.create(role);
            return "redirect:/roles";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("role", "duplicate_role", e.getMessage());
            return "role-new";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/update")
    public String showEditRolePage(Model model, @PathVariable int id) {
        try {
            Role role = roleService.getById(id);
            model.addAttribute("roleId", id);
            model.addAttribute("role", role);
            return "role-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/{id}/update")
    public String updateRole(Model model,
                             @Valid @ModelAttribute("role") Role role, BindingResult errors) {
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> model.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            roleService.update(role);
            return "redirect:/roles";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/delete")
    public String deleteRole(Model model, @PathVariable int id) {
        try {
            roleService.delete(id);
            return "redirect:/roles";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}