package com.telerikacademy.smartgaragegoodoldbilly.controllers.mvc;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.models.Brand;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.ModelDto;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.BrandService;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.ModelService;
import com.telerikacademy.smartgaragegoodoldbilly.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/models")
public class ModelMvcController {

    private final ModelService modelService;
    private final ModelMapper modelMapper;
    private final BrandService brandService;

    @Autowired
    public ModelMvcController(ModelService modelService, ModelMapper modelMapper, BrandService brandService) {
        this.modelService = modelService;
        this.modelMapper = modelMapper;
        this.brandService = brandService;
    }

    @ModelAttribute("brands")
    public List<Brand> populateStyles() {
        return brandService.getAll();
    }

    @GetMapping
    public String showAllModels(Model model) {
        model.addAttribute("models", modelService.getAll());
        return "models";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/id/{id}")
    public String showSingleModel(Model model, @PathVariable int id) {
        try {
            model.addAttribute("model", modelService.getById(id));
            return "model";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/model/{model}")
    public String showSingleModel(Model Mmodel, @PathVariable String model) {
        try {
            Mmodel.addAttribute("model", modelService.getByModel(model));
            return "model";
        } catch (EntityNotFoundException e) {
            Mmodel.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/new")
    public String showNewModelPage(Model model) {
        model.addAttribute("model", new ModelDto());
        return "model-new";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/new")
    public String createModel(@Valid @ModelAttribute("modelDtoIn") ModelDto modelDto, BindingResult errors, Model modelModel) {
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> modelModel.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            com.telerikacademy.smartgaragegoodoldbilly.models.Model model = modelMapper.fromDto(modelDto);
            modelService.create(model);
            return "redirect:/models";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("model", "duplicate_model", e.getMessage());
            return "model-new";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/update")
    public String showEditModelPage(Model model, @PathVariable int id) {
        try {
            com.telerikacademy.smartgaragegoodoldbilly.models.Model Mmodel = modelService.getById(id);
            model.addAttribute("modelId", id);
            model.addAttribute("model", Mmodel);
            return "model-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/{id}/update")
    public String updateModel(Model model,
                              @Valid @ModelAttribute("model") com.telerikacademy.smartgaragegoodoldbilly.models.Model
                                      Mmodel, BindingResult errors) {
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> model.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            modelService.update(Mmodel);
            return "redirect:/models";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/delete")
    public String deleteModel(Model model, @PathVariable int id) {
        try {
            modelService.delete(id);
            return "redirect:/models";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}