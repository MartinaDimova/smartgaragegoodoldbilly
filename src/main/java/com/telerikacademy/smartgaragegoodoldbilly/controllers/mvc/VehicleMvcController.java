package com.telerikacademy.smartgaragegoodoldbilly.controllers.mvc;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.WrongUserInput;
import com.telerikacademy.smartgaragegoodoldbilly.models.Brand;
import com.telerikacademy.smartgaragegoodoldbilly.models.User;
import com.telerikacademy.smartgaragegoodoldbilly.models.Vehicle;
import com.telerikacademy.smartgaragegoodoldbilly.models.Visit;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.VehicleDto;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.VehicleSortOptions;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/vehicles")
public class VehicleMvcController {

    private final VehicleService vehicleService;
    private final ModelService modelService;
    private final BrandService brandService;
    private final UserService userService;
    private final VisitService visitService;

    @Autowired
    public VehicleMvcController(VehicleService vehicleService, ModelService modelService, BrandService brandService, UserService userService, VisitService visitService) {
        this.vehicleService = vehicleService;
        this.modelService = modelService;
        this.brandService = brandService;
        this.userService = userService;
        this.visitService = visitService;
    }

    @ModelAttribute("brands")
    public List<Brand> populateBrands() {
        return brandService.getAll();
    }

    @ModelAttribute("brand")
    private Brand populateBrand() {
        return new Brand();
    }

    @ModelAttribute("models")
    public List<com.telerikacademy.smartgaragegoodoldbilly.models.Model> populateModels() {
        return modelService.getAll();
    }

    @ModelAttribute("model")
    public com.telerikacademy.smartgaragegoodoldbilly.models.Model populateModel() {
        return new com.telerikacademy.smartgaragegoodoldbilly.models.Model();
    }

    @ModelAttribute("users")
    public List<User> populateUsers() {
        return userService.getAll();
    }

    @ModelAttribute("user")
    public User populateUser() {
        return new User();
    }

    @ModelAttribute("vehicleDto")
    public VehicleDto populateVehicleDto() {
        return new VehicleDto();
    }

    @ModelAttribute("vehicle")
    public Vehicle populateVehicle() {
        return new Vehicle();
    }

    @ModelAttribute("sortOptions")
    public VehicleSortOptions[] populateSortOptions() {
        return VehicleSortOptions.values();
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/search")
    public String searchVehicles(Model model, @RequestParam(value = "search", required = false)
            Optional<String> search) {
        try {
            model.addAttribute("vehicles", vehicleService.searchByLicensePlateVinTelNumber(search));
            return "vehicles";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/filterByUsername")
    public String filterByUsername(Model model, @RequestParam(value = "username", required = false) Optional<String> username) {
        try {
            model.addAttribute("username", username);
            model.addAttribute("vehicles", vehicleService.filterByUsername(username));
            return "vehicles";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/filterByBrand")
    public String filterByBrand(Model model, @RequestParam(value = "brand", required = false) Optional<String> brand) {
        try {
            model.addAttribute("brand", brand);
            model.addAttribute("vehicles", vehicleService.filterByBrand(brand));
            return "vehicles";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/userFilterByBrand")
    public String userFilterByBrand(Model model, @RequestParam("vehicleId") int vehicleId,
                                    Principal principal) {
        List<Vehicle> userVehicles = vehicleService.filterByUsername(Optional.ofNullable(principal.getName()));
        List<Visit> userVisits = visitService.getAllForCustomers().stream().filter(v -> v.getVehicle().getId() == vehicleId).collect(Collectors.toList());

        try {
            model.addAttribute("vehicle", vehicleService.getById(vehicleId));
            model.addAttribute("userVehicles", userVehicles);
            System.out.println(userVehicles);
            model.addAttribute("userVisits", userVisits);
            return "index-customer";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/filterByModel")
    public String filterByModel(Model model, @RequestParam(value = "model", required = false) Optional<String> mModel) {
        try {
            model.addAttribute("model", mModel);
            model.addAttribute("vehicles", vehicleService.filterByModel(mModel));
            return "vehicles";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/filterByYear")
    public String filterByYear(Model model, @RequestParam(value = "year", required = false) Optional<String> year) {
        try {
            model.addAttribute("year", year);
            model.addAttribute("vehicles", vehicleService.filterByYear(year));
            return "vehicles";
        } catch (EntityNotFoundException | NumberFormatException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/sort")
    public String sortVehicles(Model model, @RequestParam Optional<VehicleSortOptions> sortOptions) {
        try {
            model.addAttribute("vehicles", vehicleService.sortVehicles(sortOptions));
            return "vehicles";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping
    public String showAllVehicles(Model model,
                                  @ModelAttribute("brand") Brand brand,
                                  @ModelAttribute("model") com.telerikacademy.smartgaragegoodoldbilly.models.Model vehicleModel) {
        List<Brand> allBrands = brandService.getAll();
        model.addAttribute("allBrands", allBrands);
        model.addAttribute("vehicles", vehicleService.getAll());
        Vehicle vehicle = populateVehicle();
        model.addAttribute("vehicle", vehicle);
        return "vehicles";
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/license_plate/{license_plate}")
    public String showSingleVehicle(Model model, @PathVariable String license_plate) {
        try {
            model.addAttribute("vehicle", vehicleService.getByLicensePlate(license_plate));
            return "vehicles";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/update")
    public String showEditVehiclePage(Model model, @PathVariable int id) {
        try {
            Vehicle vehicle = vehicleService.getById(id);
            model.addAttribute("vehicleId", id);
            model.addAttribute("vehicle", vehicle);
            return "redirect:/vehicles";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @PostMapping("/{id}/update")
    public String updateVehicle(Model model, @ModelAttribute("user_id") int user_id, @Valid @ModelAttribute("vehicle") Vehicle vehicle,
                                BindingResult errors) {
        User user = userService.getById(user_id);
        vehicle.setUser(user);
        if (errors.hasErrors()) {
            errors.getFieldErrors()
                    .forEach(f -> model.addAttribute("error", f.getField() + ": " + f.getDefaultMessage()));
            return "not-found";
        }
        try {
            vehicleService.update(vehicle);
            return "redirect:/vehicles";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PreAuthorize("hasAuthority('EMPLOYEE')")
    @GetMapping("/{id}/delete")
    public String deleteVehicle(Model model, @PathVariable int id) {
        try {
            vehicleService.delete(id);
            return "vehicles";
        } catch (EntityNotFoundException | NotAllowedOperation e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewVehiclePage(Model model, @ModelAttribute("brand") Brand brand,
                                     @ModelAttribute("model") Model vehicleModel) {
        model.addAttribute("vehicle", new Vehicle());
        return "vehicles";
    }

    @PostMapping("/new")
    public String createVehicle(@Valid @ModelAttribute("vehicle") Vehicle vehicle,
                                @ModelAttribute("brandId") Brand brandId,
                                @ModelAttribute("userId") int userId,
                                @ModelAttribute("modelName") String modelName, Model model) {
        com.telerikacademy.smartgaragegoodoldbilly.models.Model vehicleModel = new com.telerikacademy.smartgaragegoodoldbilly.models.Model();
        vehicleModel.setModel(modelName);
        vehicleModel.setBrand(brandService.getById(Integer.parseInt(brandId.getBrand())));
        vehicle.setModel(vehicleModel);
        vehicleModel.addVehicle(vehicle);
        User user = userService.getById(userId);
        vehicle.setUser(user);
        try {
            vehicleService.create(vehicle);
            user.addVehicle(vehicle);
            return "redirect:/vehicles";
        } catch (DuplicateEntityException | EntityNotFoundException | WrongUserInput e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/userNew")
    public String userCreateVehicle(@Valid @ModelAttribute("vehicle") Vehicle vehicle,
                                    @ModelAttribute("brandId") Brand brandId,
                                    @ModelAttribute("modelName") String modelName,
                                    Model model, Principal principal) {
        com.telerikacademy.smartgaragegoodoldbilly.models.Model userVehicleModel = new com.telerikacademy.smartgaragegoodoldbilly.models.Model();
        userVehicleModel.setModel(modelName);
        userVehicleModel.setBrand(brandService.getById(Integer.parseInt(brandId.getBrand())));
        vehicle.setModel(userVehicleModel);
        userVehicleModel.addVehicle(vehicle);
        User user = userService.getByUsername(principal.getName());
        vehicle.setUser(user);
        try {
            vehicleService.create(vehicle);
            user.addVehicle(vehicle);
            return "redirect:/index-customer";
        } catch (DuplicateEntityException | EntityNotFoundException | WrongUserInput e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/brands")
    public @ResponseBody
    List<Brand> findAllBrands() {
        return brandService.getAll();
    }

    @RequestMapping(value = "/models", method = RequestMethod.GET)
    public @ResponseBody
    Set<com.telerikacademy.smartgaragegoodoldbilly.models.Model> findAllBrandModels(@RequestParam(value = "brandId", required = true) int brandId) {
        Brand brand = brandService.getById(brandId);
        return brand.getBrandModels();
    }

}