package com.telerikacademy.smartgaragegoodoldbilly.services;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.models.Model;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.ModelRepository;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelServiceImpl implements ModelService {

    private final ModelRepository modelRepository;

    @Autowired
    public ModelServiceImpl(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    @Override
    public List<Model> getAll() {
        return modelRepository.getAll();
    }

    @Override
    public Model getById(int id) {
        return modelRepository.getById(id);
    }

    @Override
    public Model getByModel(String model) {
        return modelRepository.getByModel(model);
    }

    @Override
    public Model create(Model model) {
        List<Model> models = modelRepository.getAll();
        for (Model eachModel : models) {
            if (eachModel.getModel().equalsIgnoreCase(model.getModel())) {
                throw new DuplicateEntityException("Model", "model", model.getModel());
            }
        }
        return modelRepository.create(model);
    }

    @Override
    public Model update(Model model) {
        return modelRepository.update(model);
    }

    @Override
    public void delete(int id) {
        Model model = getById(id);
        if (model.getModelVehicles().size() != 0) {
            throw new NotAllowedOperation(NotAllowedOperation.NOT_EMPTY_OBJECT);
        }
        modelRepository.delete(id);
    }
}
