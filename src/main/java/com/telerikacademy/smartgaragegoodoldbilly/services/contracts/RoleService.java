package com.telerikacademy.smartgaragegoodoldbilly.services.contracts;

import com.telerikacademy.smartgaragegoodoldbilly.models.Role;

import java.util.List;

public interface RoleService {

    List<Role> getAll();

    Role getById(int id);

    Role getByRole(String role);

    Role create(Role role);

    Role update(Role role);

    void delete(int id);

}
