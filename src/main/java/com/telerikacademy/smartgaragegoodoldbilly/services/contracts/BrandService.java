package com.telerikacademy.smartgaragegoodoldbilly.services.contracts;

import com.telerikacademy.smartgaragegoodoldbilly.models.Brand;

import java.util.List;

public interface BrandService {

    List<Brand> getAll();

    Brand getById(int id);

    Brand getByBrand(String brand);

    Brand create(Brand brand);

    Brand update(Brand brand);

    void delete(int id);

}
