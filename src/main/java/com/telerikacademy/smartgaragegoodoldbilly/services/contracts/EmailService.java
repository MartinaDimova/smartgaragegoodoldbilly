package com.telerikacademy.smartgaragegoodoldbilly.services.contracts;

public interface EmailService {

    void send(String to, String email);

    void sendTempPassword(String to, String tempPassword);
}
