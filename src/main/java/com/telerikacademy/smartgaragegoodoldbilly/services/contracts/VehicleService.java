package com.telerikacademy.smartgaragegoodoldbilly.services.contracts;

import com.telerikacademy.smartgaragegoodoldbilly.models.Vehicle;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.VehicleSortOptions;

import java.util.List;
import java.util.Optional;

public interface VehicleService {

    List<Vehicle> searchByLicensePlateVinTelNumber(Optional<String> search);

    List<Vehicle> filterByUsername(Optional<String> filter);

    List<Vehicle> filterByBrand(Optional<String> brand);

    List<Vehicle> filterByModel(Optional<String> model);

    List<Vehicle> filterByYear(Optional<String> year);

    List<Vehicle> sortVehicles(Optional<VehicleSortOptions> sortOptions);

    List<Vehicle> getAll();

    Vehicle getById(int id);

    Vehicle getByLicensePlate(String license_plate);

    Vehicle create(Vehicle vehicle);

    Vehicle update(Vehicle vehicle);

    void delete(int id);

    void setUser(int vehicle_id, int user_id);

}
