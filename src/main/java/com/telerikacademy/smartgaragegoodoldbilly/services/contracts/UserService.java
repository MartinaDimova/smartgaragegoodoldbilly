package com.telerikacademy.smartgaragegoodoldbilly.services.contracts;

import com.telerikacademy.smartgaragegoodoldbilly.models.User;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.UserSortOptions;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> search(Optional<String> search);

    List<User> filter(Optional<Date> dateFrom, Optional<Date> dateTo,
                      Optional<UserSortOptions> sortOptions);

    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    User getByEmail(String email);

    User create(User user);

    User update(User user);

    void delete(int id);

    String signUpUser(User user);

    void enableAppUser(String email);

//    Page<User> findPage(int currentPage);



}
