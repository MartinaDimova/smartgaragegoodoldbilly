package com.telerikacademy.smartgaragegoodoldbilly.services.contracts;

import com.posadskiy.currencyconverter.enums.Currency;
import com.telerikacademy.smartgaragegoodoldbilly.models.Visit;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.VisitDtoOut;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface VisitService {

    List<Visit> filter(Optional<Date> date);

    List<Visit> getAll();

    List<Visit> getAllForCustomers();

    Visit getById(int id);

    Visit getByDate(Date date);

    Visit create(Visit visit);

    Visit update(Visit visit);

    void delete(int id);

    Double convertTotalCost(Currency from, Currency to);

    void setUser(int visit_id);

    void addService(int visit_id, Optional<Integer> service_id);

    void removeService(int visit_id, Optional<Integer> service_id);

    VisitDtoOut makeReportNewCurrency(VisitDtoOut visitDtoOut, Currency currency);
}
