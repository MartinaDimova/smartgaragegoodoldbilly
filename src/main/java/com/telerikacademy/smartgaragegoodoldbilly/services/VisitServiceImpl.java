package com.telerikacademy.smartgaragegoodoldbilly.services;

import com.posadskiy.currencyconverter.CurrencyConverter;
import com.posadskiy.currencyconverter.config.ConfigBuilder;
import com.posadskiy.currencyconverter.enums.Currency;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgaragegoodoldbilly.models.HistoricalService;
import com.telerikacademy.smartgaragegoodoldbilly.models.Visit;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.VisitDtoOut;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.VisitRepository;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;

@Service
public class VisitServiceImpl implements VisitService {

    private final VisitRepository visitRepository;
    public static final String CURRENCY_LAYER = "3458e66956d99864b4b5753a17ae45f9";
    public static final String CURRENCY_CONVERTER_API_API_KEY = "5fc8cd89d630a0500f04";

    public List<Visit> filter(Optional<Date> date) {
        return visitRepository.filter(date);
    }

    @Autowired
    public VisitServiceImpl(VisitRepository visitRepository) {
        this.visitRepository = visitRepository;
    }

    @Override
    public List<Visit> getAll() {
        return visitRepository.getAll();
    }

    @Override
    public List<Visit> getAllForCustomers() {
        return visitRepository.getAllForCustomers();
    }

    @Override
    public Visit getById(int id) {
        return visitRepository.getById(id);
    }

    @Override
    public Visit getByDate(Date date) {
        return visitRepository.getByDate(date);
    }

    @Override
    public Visit create(Visit visit) {
        return visitRepository.create(visit);
    }

    @Override
    public Visit update(Visit visit) {
        return visitRepository.update(visit);
    }

    @Override
    public void delete(int id) {
        Visit visit = getById(id);
        if (visit.getTotal_cost() == null || visit.getTotal_cost() <= 0) {
            visitRepository.delete(id);
        } else throw new NotAllowedOperation(NotAllowedOperation.NOT_EMPTY_OBJECT);
    }

    @Override
    public Double convertTotalCost(Currency from, Currency to) {
        try {
            CurrencyConverter converter = new CurrencyConverter(
                    new ConfigBuilder()
                            .currencyLayerApiKey(CURRENCY_LAYER)
                            .currencyConverterApiApiKey(CURRENCY_CONVERTER_API_API_KEY)
                            .build()
            );
            return converter.rate(Currency.BGN, to);
        } catch (RuntimeException e) {
            throw new UnauthorizedOperationException(UnauthorizedOperationException.NO_SERVICE);
        }
    }

    @Override
    public VisitDtoOut makeReportNewCurrency(VisitDtoOut visitDtoOut, Currency currency) {
        DecimalFormat df = new DecimalFormat("#.##");
        double exchangeRate = convertTotalCost(Currency.BGN, currency);
        visitDtoOut.setTotal_cost(Double.parseDouble(df.format(exchangeRate * visitDtoOut.getTotal_cost())));
        for (HistoricalService service : visitDtoOut.getHVisitServices()) {
            service.setPrice(Double.parseDouble(df.format(exchangeRate * service.getPrice())));
        }
        visitDtoOut.setCurrency(currency.toString());
        return visitDtoOut;
    }

    @Override
    public void setUser(int visit_id) {
        visitRepository.setUser(visit_id);
    }

    @Override
    public void addService(int visit_id, Optional<Integer> service_id) {
        visitRepository.addService(visit_id, service_id);
    }

    @Override
    public void removeService(int visit_id, Optional<Integer> service_id) {
        visitRepository.removeService(visit_id, service_id);
    }
}