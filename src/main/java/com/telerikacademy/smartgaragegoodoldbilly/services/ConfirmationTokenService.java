package com.telerikacademy.smartgaragegoodoldbilly.services;

import com.telerikacademy.smartgaragegoodoldbilly.models.ConfirmationToken;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.ConfirmationTokenRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class ConfirmationTokenService {

    private final ConfirmationTokenRepository confirmationTokenRepository;

    public ConfirmationTokenService(ConfirmationTokenRepository confirmationTokenRepository) {
        this.confirmationTokenRepository = confirmationTokenRepository;
    }

    public void saveConfirmationToken(ConfirmationToken token) {
        confirmationTokenRepository.create(token);
    }

    public Optional<ConfirmationToken> getToken(String token) {
        return Optional.ofNullable(confirmationTokenRepository.findByToken(token));
    }

    public void setConfirmedAt(String token) {
        confirmationTokenRepository.updateConfirmedAt(token, LocalDateTime.now());
    }
}
