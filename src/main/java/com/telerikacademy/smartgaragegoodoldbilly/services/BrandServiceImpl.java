package com.telerikacademy.smartgaragegoodoldbilly.services;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.models.Brand;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.BrandRepository;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {

    private final BrandRepository brandRepository;

    @Autowired
    public BrandServiceImpl(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }

    @Override
    public List<Brand> getAll() {
        return brandRepository.getAll();
    }

    @Override
    public Brand getById(int id) {
        return brandRepository.getById(id);
    }

    @Override
    public Brand getByBrand(String brand) {
        return brandRepository.getByBrand(brand);
    }

    @Override
    public Brand create(Brand brand) {
        List<Brand> brands = brandRepository.getAll();
        for (Brand eachBrand : brands) {
            if (eachBrand.getBrand().equalsIgnoreCase(brand.getBrand())) {
                throw new DuplicateEntityException("Brand", "brand", brand.getBrand());
            }
        }
        return brandRepository.create(brand);
    }

    @Override
    public Brand update(Brand brand) {
        return brandRepository.update(brand);
    }

    @Override
    public void delete(int id) {
        Brand brand = getById(id);
        if (brand.getBrandModels().size() != 0) {
            throw new NotAllowedOperation(NotAllowedOperation.NOT_EMPTY_OBJECT);
        }
        brandRepository.delete(id);
    }
}
