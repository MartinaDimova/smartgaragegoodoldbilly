package com.telerikacademy.smartgaragegoodoldbilly.services;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.models.Role;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.RoleRepository;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Role> getAll() {
        return roleRepository.getAll();
    }

    @Override
    public Role getById(int id) {
        return roleRepository.getById(id);
    }

    @Override
    public Role getByRole(String role) {
        return roleRepository.getByRole(role);
    }

    @Override
    public Role create(Role role) {
        List<Role> roles = roleRepository.getAll();
        for (Role eachRole : roles) {
            if (eachRole.getRole().equalsIgnoreCase(role.getRole())) {
                throw new DuplicateEntityException("Role", "role", role.getRole());
            }
        }
        return roleRepository.create(role);
    }

    @Override
    public Role update(Role role) {
        return roleRepository.update(role);
    }

    @Override
    public void delete(int id) {
        Role role = getById(id);
        if (role.getRoleUsers().size() != 0) {
            throw new NotAllowedOperation(NotAllowedOperation.NOT_EMPTY_OBJECT);
        }
        roleRepository.delete(id);
    }
}