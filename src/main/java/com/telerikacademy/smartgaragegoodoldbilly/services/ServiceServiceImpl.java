package com.telerikacademy.smartgaragegoodoldbilly.services;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.models.HistoricalService;
import com.telerikacademy.smartgaragegoodoldbilly.models.Service;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.ServicePriceRangeOptions;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.ServiceSortOptions;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.HistoricalServiceRepository;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.ServiceRepository;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {

    private final ServiceRepository serviceRepository;
    private final HistoricalServiceRepository historicalServiceRepository;

    @Autowired
    public ServiceServiceImpl(ServiceRepository serviceRepository, HistoricalServiceRepository historicalServiceRepository) {
        this.serviceRepository = serviceRepository;
        this.historicalServiceRepository = historicalServiceRepository;
    }

    @Override
    public List<Service> search(Optional<String> search) {
        return serviceRepository.search(search);
    }

    @Override
    public List<Service> filter(Optional<ServicePriceRangeOptions> price, Optional<ServiceSortOptions> sortOptions) {
        return serviceRepository.filter(price, sortOptions);
    }

    @Override
    public List<Service> getAll() {
        return serviceRepository.getAll();
    }

    @Override
    public Service getById(int id) {
        return serviceRepository.getById(id);
    }

    @Override
    public Service getByService(String service) {
        return serviceRepository.getByService(service);
    }

    @Override
    public Service create(Service service) {
        List<Service> services = serviceRepository.getAll();
        for (Service eachService : services) {
            if (eachService.getService().equalsIgnoreCase(service.getService())) {
                throw new DuplicateEntityException("Service", "service", service.getService());
            }
        }
        HistoricalService historicalService = new HistoricalService();
        historicalService.setService(service.getService());
        historicalService.setPrice(service.getPrice());
        historicalServiceRepository.create(historicalService);
        return serviceRepository.create(service);
    }

    @Override
    public Service update(Service service) {
        Service oldService = serviceRepository.getById(service.getId());
        HistoricalService historicalService = new HistoricalService();
        if (oldService.getPrice() != service.getPrice()) {
            historicalService.setService(service.getService());
            historicalService.setPrice(oldService.getPrice());
            historicalServiceRepository.create(historicalService);
        } else {
            historicalService.setService(service.getService());
            historicalService.setPrice(oldService.getPrice());
            historicalServiceRepository.update(historicalService);
        }
        return serviceRepository.update(service);
    }

    @Override
    public void delete(int id) {
        Service service = getById(id);
        if (service.getServiceVisits() != null || service.getServiceVisits().size() != 0) {
            throw new NotAllowedOperation(NotAllowedOperation.NOT_EMPTY_OBJECT);
        }
        serviceRepository.delete(id);
    }
}
