package com.telerikacademy.smartgaragegoodoldbilly.services;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.models.ConfirmationToken;
import com.telerikacademy.smartgaragegoodoldbilly.models.Role;
import com.telerikacademy.smartgaragegoodoldbilly.models.User;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.UserSortOptions;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.RoleRepository;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.UserRepository;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ConfirmationTokenService confirmationTokenService;
    private final RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, ConfirmationTokenService confirmationTokenService, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.confirmationTokenService = confirmationTokenService;
        this.roleRepository = roleRepository;
    }

    @Override
    public List<User> search(Optional<String> search) {
            return userRepository.search(search);
    }

    @Override
    public List<User> filter(Optional<Date> dateFrom, Optional<Date> dateTo, Optional<UserSortOptions> sortOptions) {
        return userRepository.filter(dateFrom, dateTo, sortOptions);
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public User create(User user) {
        String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        return userRepository.create(user);
    }

    @Override
    public User update(User user) {
        String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        user.setEnabled(true);
        User oldUser = getById(user.getId());
        Set<User> roleUsers = new HashSet<>();
        for (Role role : oldUser.getUserRoles()) {
            roleUsers.addAll(role.getRoleUsers());
            roleRepository.update(role);
        }
        Set<Role> userRoles = new HashSet<>();
        userRoles.addAll(oldUser.getUserRoles());
        user.setUserRoles(userRoles);
        return userRepository.update(user);
    }

    @Override
    public void delete(int id) {
        User user = getById(id);
        if (user.getUserVehicles().size() != 0) {
            throw new NotAllowedOperation(NotAllowedOperation.NOT_EMPTY_OBJECT);
        }
        userRepository.delete(id);
    }

    @Override
    @Transactional
    @Modifying
    public void enableAppUser(String email) {
        userRepository.enableAppUser(email);
    }

    @Override
    public String signUpUser(User user) {

        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusHours(24),
                user
        );
        confirmationTokenService.saveConfirmationToken(confirmationToken);
        return token;
    }
}