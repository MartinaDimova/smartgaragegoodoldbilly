package com.telerikacademy.smartgaragegoodoldbilly.services;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.NotAllowedOperation;
import com.telerikacademy.smartgaragegoodoldbilly.models.Vehicle;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.VehicleSortOptions;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.VehicleRepository;
import com.telerikacademy.smartgaragegoodoldbilly.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public List<Vehicle> searchByLicensePlateVinTelNumber(Optional<String> search) {
        return vehicleRepository.searchByLicensePlateVinTelNumber(search);
    }

    @Override
    public List<Vehicle> filterByUsername(Optional<String> username) {
        return vehicleRepository.filterByUsername(username);
    }

    @Override
    public List<Vehicle> filterByBrand(Optional<String> brand) {
        return vehicleRepository.filterByBrand(brand);
    }

    @Override
    public List<Vehicle> filterByModel(Optional<String> model) {
        return vehicleRepository.filterByModel(model);
    }

    @Override
    public List<Vehicle> filterByYear(Optional<String> year) {
        return vehicleRepository.filterByYear(year);
    }

    @Override
    public List<Vehicle> sortVehicles(Optional<VehicleSortOptions> sortOptions) {
        return vehicleRepository.sortVehicles(sortOptions);
    }

    @Override
    public List<Vehicle> getAll() {
        return vehicleRepository.getAll();
    }

    @Override
    public Vehicle getById(int id) {
        return vehicleRepository.getById(id);
    }

    @Override
    public Vehicle getByLicensePlate(String license_plate) {
        return vehicleRepository.getByLicensePlate(license_plate);
    }

    @Override
    public Vehicle create(Vehicle vehicle) {
        List<Vehicle> vehicles = vehicleRepository.getAll();
        for (Vehicle eachVehicle : vehicles) {
            if (eachVehicle.getVin().equalsIgnoreCase(vehicle.getVin())) {
                throw new DuplicateEntityException("Vehicle", "VIN", vehicle.getVin());
            } else if (eachVehicle.getLicense_plate().equalsIgnoreCase(vehicle.getLicense_plate())) {
                throw new DuplicateEntityException("Vehicle", "license plate", vehicle.getLicense_plate());
            }
        }
        return vehicleRepository.create(vehicle);
    }

    @Override
    public Vehicle update(Vehicle vehicle) {
        return vehicleRepository.update(vehicle);
    }

    @Override
    public void delete(int id) {
        Vehicle vehicle = getById(id);
        if (vehicle.getVehicleVisits().size() != 0) {
            throw new NotAllowedOperation(NotAllowedOperation.NOT_EMPTY_OBJECT);
        }
        vehicleRepository.delete(id);
    }

    @Override
    public void setUser(int vehicle_id, int user_id) {
        vehicleRepository.setUser(vehicle_id, user_id);
    }
}
