package com.telerikacademy.smartgaragegoodoldbilly.repositories;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.models.HistoricalService;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.HistoricalServiceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HistoricalServiceRepositoryImpl implements HistoricalServiceRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public HistoricalServiceRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public HistoricalService getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            HistoricalService service = session.get(HistoricalService.class, id);
            if (service == null) {
                throw new EntityNotFoundException("Service", id);
            }
            return service;
        }
    }

    @Override
    public HistoricalService getByService(String service) {
        try (Session session = sessionFactory.openSession()) {
            Query<HistoricalService> query = session.createQuery("from HistoricalService where service = :service", HistoricalService.class);
            query.setParameter("service", service);
            List<HistoricalService> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Service", "service", service);
            }
            return result.get(0);
        }
    }

    @Override
    public HistoricalService create(HistoricalService service) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(service);
            session.getTransaction().commit();
        }
        return service;
    }

    @Override
    public HistoricalService update(HistoricalService service) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(service);
            session.getTransaction().commit();
        }
        return service;
    }

    @Override
    public void delete(int id) {
        HistoricalService service = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(service);
            session.getTransaction().commit();
        }
    }
}
