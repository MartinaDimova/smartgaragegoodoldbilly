package com.telerikacademy.smartgaragegoodoldbilly.repositories;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.models.Brand;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.BrandRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BrandRepositoryImpl implements BrandRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public BrandRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brand> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Brand> query = session.createQuery("from Brand ", Brand.class);
            return query.list();
        }
    }

    @Override
    public Brand getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brand brand = session.get(Brand.class, id);
            if (brand == null) {
                throw new EntityNotFoundException("Brand", id);
            }
            return brand;
        }
    }

    @Override
    public Brand getByBrand(String brand) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brand> query = session.createQuery("from Brand where brand = :brand", Brand.class);
            query.setParameter("brand", brand);
            List<Brand> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Brand", "brand", brand);
            }
            return result.get(0);
        }
    }

    @Override
    public Brand create(Brand brand) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(brand);
            session.getTransaction().commit();
        }
        return brand;
    }

    @Override
    public Brand update(Brand brand) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(brand);
            session.getTransaction().commit();
        }
        return brand;
    }

    @Override
    public void delete(int id) {
        Brand brand = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(brand);
            session.getTransaction().commit();
        }
    }
}