package com.telerikacademy.smartgaragegoodoldbilly.repositories;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.models.User;
import com.telerikacademy.smartgaragegoodoldbilly.models.Vehicle;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.VehicleSortOptions;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.UserRepository;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.VehicleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class VehicleRepositoryImpl implements VehicleRepository {

    private final SessionFactory sessionFactory;
    private final UserRepository userRepository;

    @Autowired
    public VehicleRepositoryImpl(SessionFactory sessionFactory, UserRepository userRepository) {
        this.sessionFactory = sessionFactory;
        this.userRepository = userRepository;
    }

    @Override
    public List<Vehicle> searchByLicensePlateVinTelNumber(Optional<String> search) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery(
                    "from Vehicle where license_plate like :name" +
                            " or vin like :name", Vehicle.class);
            query.setParameter("name", "%" + search.get() + "%");
            if (query.list().isEmpty()) {
                NativeQuery<Vehicle> query1 = session.createNativeQuery(
                        "select v.vehicle_id, v.license_plate, v.vin, v.year, v.model_id, v.user_id  " +
                                "from vehicles v " +
                                "join users u on v.user_id = u.user_id " +
                                "where u.phone_number = :name ");
                query1.setParameter("name", search.get());
                query1.addEntity(Vehicle.class);
                query = query1;
            }
            return query.list();
        }
    }

    @Override
    public List<Vehicle> filterByUsername(Optional<String> username) {
        if (username.isPresent()) {
            try (Session session = sessionFactory.openSession()) {
                NativeQuery<Vehicle> query = session.createNativeQuery(
                        "select v.vehicle_id, v.license_plate, v.vin, v.year, v.model_id, v.user_id  " +
                                "from vehicles v " +
                                "join users u on v.user_id = u.user_id " +
                                "where u.username = :name");
                query.setParameter("name", username.get());
                query.addEntity(Vehicle.class);
                return query.list();
            }
        } else return getAll();
    }

    @Override
    public List<Vehicle> filterByBrand(Optional<String> brand) {
        if (brand.isPresent()) {
            try (Session session = sessionFactory.openSession()) {
                NativeQuery<Vehicle> query = session.createNativeQuery(
                        "select v.vehicle_id, v.license_plate, v.vin, v.year, v.model_id, v.user_id  " +
                                "from vehicles v " +
                                "join models m on v.model_id = m.model_id " +
                                "join brands b on m.brand_id = b.brand_id " +
                                "where b.brand = :name");
                query.setParameter("name", brand.get());
                query.addEntity(Vehicle.class);
                return query.list();
            }
        } else return getAll();
    }

    @Override
    public List<Vehicle> filterByModel(Optional<String> model) {
        if (model.isPresent()) {
            try (Session session = sessionFactory.openSession()) {
                NativeQuery<Vehicle> query = session.createNativeQuery(
                        "select v.vehicle_id, v.license_plate, v.vin, v.year, v.model_id, v.user_id  " +
                                "from vehicles v " +
                                "join models m on v.model_id = m.model_id " +
                                "where m.model = :name");
                query.setParameter("name", model.get());
                query.addEntity(Vehicle.class);
                return query.list();
            }
        } else return getAll();
    }

    @Override
    public List<Vehicle> filterByYear(Optional<String> year) {
        if (year.isPresent() || year.equals("")) {
            try (Session session = sessionFactory.openSession()) {
                Query<Vehicle> query = session.createQuery(
                        "from Vehicle where year= :name", Vehicle.class);
                query.setParameter("name", Integer.valueOf(year.get()));
                return query.list();
            }
        } else return getAll();
    }

    @Override
    public List<Vehicle> sortVehicles(Optional<VehicleSortOptions> sortOptions) {
        try (Session session = sessionFactory.openSession()) {
            var baseQuery = new StringBuilder("select * " +
                    "from vehicles v join users u on v.user_id = u.user_id join models m " +
                    "on v.model_id = m.model_id join brands b on m.brand_id = b.brand_id ");
            sortOptions.ifPresent(value -> baseQuery.append(value.getQuery()));
            NativeQuery<Vehicle> query = session.createNativeQuery(baseQuery.toString());
            query.addEntity(Vehicle.class);
            return query.list();
        }
    }

    @Override
    public List<Vehicle> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery("from Vehicle ", Vehicle.class);
            return query.list();
        }
    }

    @Override
    public Vehicle getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Vehicle vehicle = session.get(Vehicle.class, id);
            if (vehicle == null) {
                throw new EntityNotFoundException("Vehicle", id);
            }
            return vehicle;
        }
    }

    @Override
    public Vehicle getByLicensePlate(String license_plate) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery("from Vehicle where license_plate = :license_plate", Vehicle.class);
            query.setParameter("license_plate", license_plate);
            List<Vehicle> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Vehicle", "vehicle", license_plate);
            }
            return result.get(0);
        }
    }

    @Override
    public Vehicle create(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(vehicle);
            session.getTransaction().commit();
        }
        return vehicle;
    }

    @Override
    public Vehicle update(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(vehicle);
            session.getTransaction().commit();
        }
        return vehicle;
    }

    @Override
    public void delete(int id) {
        Vehicle vehicle = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(vehicle);
            session.getTransaction().commit();
        }
    }

    @Override
    public void setUser(int vehicle_id, int user_id) {
        Vehicle vehicle = getById(vehicle_id);
        User user = userRepository.getById(user_id);
        vehicle.setUser(user);
        update(vehicle);
        userRepository.update(user);
    }
}