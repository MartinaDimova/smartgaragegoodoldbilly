package com.telerikacademy.smartgaragegoodoldbilly.repositories;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.models.Service;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.ServicePriceRangeOptions;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.ServiceSortOptions;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.ServiceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class ServiceRepositoryImpl implements ServiceRepository, JpaSpecificationExecutor<Service> {

    private final SessionFactory sessionFactory;

    @Autowired
    public ServiceRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Service> search(Optional<String> search) {
        try (Session session = sessionFactory.openSession()) {
            if (search.isEmpty()) {
                return getAll();
            }
            Query<Service> query = session.createQuery(
                    "from Service where service like :name or price = :name order by service", Service.class);
            query.setParameter("name", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<Service> filter(Optional<ServicePriceRangeOptions> price, Optional<ServiceSortOptions> sortOptions) {
        if (price.isEmpty() && sortOptions.isEmpty()) {
            return getAll();
        }
        try (Session session = sessionFactory.openSession()) {
            var baseQuery = new StringBuilder("from Service where ");
            var params = new HashMap<String, Object>();
            price.ifPresent(value -> baseQuery.append(value.getQuery()));
            if (baseQuery.toString().equals("from Service where ")) {
                baseQuery.replace(0, baseQuery.length() - 1, "from Service ");
            }
            sortOptions.ifPresent(value -> baseQuery.append(value.getQuery()));
            return session.createQuery(baseQuery.toString(), Service.class)
                    .setProperties(params)
                    .list();
        }
    }

    @Override
    public List<Service> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Service> query = session.createQuery("from Service ", Service.class);
            return query.list();
        }
    }

    @Override
    public Service getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Service service = session.get(Service.class, id);
            if (service == null) {
                throw new EntityNotFoundException("Service", id);
            }
            return service;
        }
    }

    @Override
    public Service getByService(String service) {
        try (Session session = sessionFactory.openSession()) {
            Query<Service> query = session.createQuery("from Service where service = :service", Service.class);
            query.setParameter("service", service);
            List<Service> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Service", "service", service);
            }
            return result.get(0);
        }
    }

    @Override
    public Service create(Service service) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(service);
            session.getTransaction().commit();
        }
        return service;
    }

    @Override
    public Service update(Service service) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(service);
            session.getTransaction().commit();
        }
        return service;
    }

    @Override
    public void delete(int id) {
        Service service = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(service);
            session.getTransaction().commit();
        }
    }

    @Override
    public Optional<Service> findOne(Specification<Service> spec) {
        return Optional.empty();
    }

    @Override
    public List<Service> findAll(Specification<Service> spec) {
        return null;
    }

    @Override
    public Page<Service> findAll(Specification<Service> spec, Pageable pageable) {
        return null;
    }

    @Override
    public List<Service> findAll(Specification<Service> spec, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<Service> spec) {
        return 0;
    }
}