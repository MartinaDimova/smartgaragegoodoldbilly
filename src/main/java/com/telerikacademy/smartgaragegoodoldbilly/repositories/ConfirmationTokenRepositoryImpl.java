package com.telerikacademy.smartgaragegoodoldbilly.repositories;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.models.ConfirmationToken;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.ConfirmationTokenRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class ConfirmationTokenRepositoryImpl implements ConfirmationTokenRepository {

    private final SessionFactory sessionFactory;

    public ConfirmationTokenRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public ConfirmationToken getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            ConfirmationToken confirmationToken = session.get(ConfirmationToken.class, id);
            if (confirmationToken == null) {
                throw new EntityNotFoundException("ConfirmationToken", id);
            }
            return confirmationToken;
        }
    }

    @Override
    public ConfirmationToken findByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            org.hibernate.query.Query<ConfirmationToken> query = session.createQuery("from ConfirmationToken " +
                    "where token = :token", ConfirmationToken.class);
            query.setParameter("token", token);
            List<ConfirmationToken> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Confirmation Token", "token", token);
            }
            return result.get(0);
        }
    }

    @Override
    @Transactional
    @Modifying
    public int updateConfirmedAt(String token,
                                 LocalDateTime confirmedAt) {
        try (Session session = sessionFactory.openSession()) {
            ConfirmationToken confirmationToken = findByToken(token);
            confirmationToken.setConfirmedAt(confirmedAt);
            session.beginTransaction();
            session.update(confirmationToken);
            session.getTransaction().commit();
        }
        return 1;
    }

    @Override
    public ConfirmationToken create(ConfirmationToken confirmationToken) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(confirmationToken);
            session.getTransaction().commit();
        }
        return confirmationToken;
    }

    @Override
    public ConfirmationToken update(ConfirmationToken confirmationToken) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(confirmationToken);
            session.getTransaction().commit();
        }
        return confirmationToken;
    }

    @Override
    public void delete(int id) {
        ConfirmationToken confirmationToken = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(confirmationToken);
            session.getTransaction().commit();
        }
    }
}
