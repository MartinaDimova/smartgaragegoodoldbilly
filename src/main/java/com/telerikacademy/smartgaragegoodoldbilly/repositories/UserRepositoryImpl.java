package com.telerikacademy.smartgaragegoodoldbilly.repositories;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.models.User;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.UserSortOptions;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> search(Optional<String> search) {
        try (Session session = sessionFactory.openSession()) {
            if (search.isEmpty()) {
                search = Optional.of("");
            }
            Query<User> query = session.createQuery(
                    "from User where username like :name or email like :name" +
                            " or phone_number like :name order by username", User.class);
            query.setParameter("name", "%" + search.get() + "%");
            if (query.list().isEmpty()) {
                NativeQuery<User> query1 = session.createNativeQuery(
                        "SELECT * FROM users WHERE user_id IN\n" +
                                "(SELECT user_id FROM vehicles WHERE vin = :name or license_plate = :name)");
                query1.setParameter("name", search.get());
                query1.addEntity(User.class);
                query = query1;
            }
            return query.getResultList();
        }
    }

    @Override
    public List<User> filter(Optional<Date> dateFrom, Optional<Date> dateTo,
                             Optional<UserSortOptions> sortOptions) {
        try (Session session = sessionFactory.openSession()) {
            var baseQuery = new StringBuilder("select * from users u inner join visits v on u.user_id = v.user_id where ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();
            dateFrom.ifPresent(value -> {
                filters.add("v.visit_date>= :dateFrom");
                params.put("dateFrom", value);
            });
            dateTo.ifPresent(value -> {
                filters.add("v.visit_date<= :dateTo");
                params.put("dateTo", value);
            });
            if (!filters.isEmpty()) {
                baseQuery.append(String.join(" and ", filters));
            } else {
                baseQuery.replace(0, baseQuery.length() - 1, "select * from users u" +
                        " left join visits v on u.user_id = v.user_id");
            }
            sortOptions.ifPresent(value -> baseQuery.append(value.getQuery()));
            NativeQuery<User> query = session.createNativeQuery(baseQuery.toString());
            dateFrom.ifPresent(date -> query.setParameter("dateFrom", date));
            dateTo.ifPresent(date -> query.setParameter("dateTo", date));
            query.addEntity(User.class);
            return query.list();
        }
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User ", User.class);
            return query.list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "user", username);
            }
            return result.get(0);
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);
            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "user", email);
            }
            return result.get(0);
        }
    }

    @Override
    public User create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            String username = user.getUsername();
            String email = user.getEmail();
            String phone_number = user.getPhone_number();
            Query<User> query = session.createQuery("from User where username like :username " +
                    "or email like :email or phone_number like :phone_number order by username", User.class);
            query.setParameter("username", "%" + username + "%");
            query.setParameter("email", "%" + email + "%");
            query.setParameter("phone_number", "%" + phone_number + "%");
            if (!query.list().isEmpty()) {
                throw new DuplicateEntityException("User", "username", user.getUsername());
            }
            session.save(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public User update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public void delete(int id) {
        User user = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void enableAppUser(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);
            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "user", email);
            }
            User user = result.get(0);
            user.setEnabled(true);
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

}