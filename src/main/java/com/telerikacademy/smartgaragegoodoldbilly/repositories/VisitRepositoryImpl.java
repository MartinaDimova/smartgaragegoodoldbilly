package com.telerikacademy.smartgaragegoodoldbilly.repositories;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgaragegoodoldbilly.models.*;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.HistoricalServiceRepository;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.ServiceRepository;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.UserRepository;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.VisitRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public class VisitRepositoryImpl implements VisitRepository {

    private final SessionFactory sessionFactory;
    private final UserRepository userRepository;
    private final ServiceRepository serviceRepository;
    private final HistoricalServiceRepository historicalServiceRepository;

    @Autowired
    public VisitRepositoryImpl(SessionFactory sessionFactory, UserRepository userRepository, ServiceRepository serviceRepository, HistoricalServiceRepository historicalServiceRepository) {
        this.sessionFactory = sessionFactory;
        this.userRepository = userRepository;
        this.serviceRepository = serviceRepository;
        this.historicalServiceRepository = historicalServiceRepository;
    }

    @Override
    public List<Visit> filter(Optional<Date> date) {
        if (date.isPresent()) {
            try (Session session = sessionFactory.openSession()) {
                Query<Visit> query = session.createQuery("from Visit where date = :date", Visit.class);
                query.setParameter("date", date.get());
                return query.list();
            }
        } else return getAll();
    }

    @Override
    public List<Visit> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery("from Visit ", Visit.class);
            return query.list();
        }
    }

    @Override
    public List<Visit> getAllForCustomers() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int user_id = 0;
        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();
            User user = userRepository.getByUsername(username);
            user_id = user.getId();
        }
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Visit> query1 = session.createNativeQuery(
                    "SELECT * FROM visits v JOIN users u on u.user_id = v.user_id where v.user_id = :user_id");
            query1.setParameter("user_id", user_id);
            query1.addEntity(Visit.class);
            return query1.list();
        }
    }


    @Override
    public Visit getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Visit visit = session.get(Visit.class, id);
            if (visit == null) {
                throw new EntityNotFoundException("Visit", id);
            }
            return visit;
        }
    }

    @Override
    public Visit getByDate(Date date) {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery("from Visit where date = :date", Visit.class);
            query.setParameter("date", date);
            List<Visit> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Visit", "visit", date.toLocalDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            }
            return result.get(0);
        }
    }

    @Override
    public Visit create(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(visit);
            session.getTransaction().commit();
        }
        return visit;
    }

    @Override
    public Visit update(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(visit);
            session.getTransaction().commit();
        }
        return visit;
    }

    @Override
    public void delete(int id) {
        Visit visit = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(visit);
            session.getTransaction().commit();
        }
    }

    @Override
    public void setUser(int visit_id) {
        Visit visit = getById(visit_id);
        Vehicle vehicle = visit.getVehicle();
        User user = vehicle.getUser();
        visit.setUser(user);
        update(visit);
        userRepository.update(user);
    }

    @Override
    public void addService(int visit_id, Optional<Integer> service_id) {
        Visit visit = getById(visit_id);
        Date date = Date.valueOf(LocalDate.now());
        if (date.after(visit.getDate())) {
            throw new UnauthorizedOperationException(UnauthorizedOperationException.TOO_LATE);
        }
        Service service = serviceRepository.getById(service_id.get());
        HistoricalService historicalService = historicalServiceRepository.getByService(service.getService());
        Set<Service> newSet = new HashSet<>();
        newSet.addAll(visit.getVisitServices());
        newSet.add(service);
        visit.setVisitServices(newSet);
        visit.sethVisitServices(newSet);
        visit.setTotal_cost();
        Set<Visit> newVisit = new HashSet<>();
        newVisit.addAll(service.getServiceVisits());
        newVisit.add(visit);
        service.setServiceVisits(newVisit);
        update(visit);
        serviceRepository.update(service);
        historicalServiceRepository.update(historicalService);
    }

    @Override
    public void removeService(int visit_id, Optional<Integer> service_id) {
        Visit visit = getById(visit_id);
        Date date = Date.valueOf(LocalDate.now());
        if (date.after(visit.getDate())) {
            throw new UnauthorizedOperationException(UnauthorizedOperationException.TOO_LATE);
        }
        Service service = serviceRepository.getById(service_id.get());
        HistoricalService historicalService = historicalServiceRepository.getByService(service.getService());
        Set<Service> newSet = new HashSet<>();
        for (Service service1 : visit.getVisitServices()) {
            if (service.getId() != service1.getId()) {
                newSet.add(service1);
            }
        }
        visit.setVisitServices(newSet);
        visit.sethVisitServices(newSet);
        visit.setTotal_cost();
        Set<Visit> newVisit = new HashSet<>();
        for (Visit visit1 : service.getServiceVisits()) {
            if (visit.getId() != visit1.getId()) {
                newVisit.add(visit1);
            }
        }
        service.setServiceVisits(newVisit);
        visit.setTotal_cost();
        update(visit);
        serviceRepository.update(service);
        historicalServiceRepository.update(historicalService);
    }
}