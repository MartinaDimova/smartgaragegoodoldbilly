package com.telerikacademy.smartgaragegoodoldbilly.repositories;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.models.Model;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.ModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ModelRepositoryImpl implements ModelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Model> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery("from Model ", Model.class);
            return query.list();
        }
    }

    @Override
    public Model getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Model model = session.get(Model.class, id);
            if (model == null) {
                throw new EntityNotFoundException("Model", id);
            }
            return model;
        }
    }

    @Override
    public Model getByModel(String model) {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery("from Model where model = :model", Model.class);
            query.setParameter("model", model);
            List<Model> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Model", "model", model);
            }
            return result.get(0);
        }
    }

    @Override
    public Model create(Model model) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(model);
            session.getTransaction().commit();
        }
        return model;
    }

    @Override
    public Model update(Model model) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(model);
            session.getTransaction().commit();
        }
        return model;
    }

    @Override
    public void delete(int id) {
        Model model = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(model);
            session.getTransaction().commit();
        }
    }
}