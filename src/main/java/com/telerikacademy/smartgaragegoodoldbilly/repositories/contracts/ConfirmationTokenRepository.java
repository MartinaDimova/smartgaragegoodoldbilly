package com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts;

import com.telerikacademy.smartgaragegoodoldbilly.models.ConfirmationToken;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

public interface ConfirmationTokenRepository {

    ConfirmationToken getById(int id);

    ConfirmationToken findByToken(String token);

    @Transactional
    @Modifying
    @Query("UPDATE ConfirmationToken c " +
            "SET c.confirmedAt = ?2 " +
            "WHERE c.token = ?1")
    int updateConfirmedAt(String token,
                          LocalDateTime confirmedAt);

    ConfirmationToken create(ConfirmationToken confirmationToken);

    ConfirmationToken update(ConfirmationToken confirmationToken);

    void delete(int id);
}
