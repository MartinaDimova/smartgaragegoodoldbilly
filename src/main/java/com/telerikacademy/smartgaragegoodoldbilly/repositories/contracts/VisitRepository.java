package com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts;

import com.telerikacademy.smartgaragegoodoldbilly.models.Visit;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface VisitRepository {

    List<Visit> filter(Optional<Date> date);

    List<Visit> getAll();

    List<Visit> getAllForCustomers();

    Visit getById(int id);

    Visit getByDate(Date date);

    Visit create(Visit visit);

    Visit update(Visit visit);

    void delete(int id);

    void setUser(int visit_id);

    void addService(int visit_id, Optional<Integer> service_id);

    void removeService(int visit_id, Optional<Integer> service_id);

}
