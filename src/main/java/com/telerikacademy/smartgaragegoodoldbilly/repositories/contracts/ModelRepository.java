package com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts;

import com.telerikacademy.smartgaragegoodoldbilly.models.Model;

import java.util.List;

public interface ModelRepository {

    List<Model> getAll();

    Model getById(int id);

    Model getByModel(String model);

    Model create(Model model);

    Model update(Model model);

    void delete(int id);

}
