package com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts;

import com.telerikacademy.smartgaragegoodoldbilly.models.Service;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.ServicePriceRangeOptions;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.ServiceSortOptions;

import java.util.List;
import java.util.Optional;

public interface ServiceRepository {

    List<Service> search(Optional<String> search);

    List<Service> filter(Optional<ServicePriceRangeOptions> price, Optional<ServiceSortOptions> sortOptions);

    List<Service> getAll();

    Service getById(int id);

    Service getByService(String service);

    Service create(Service service);

    Service update(Service service);

    void delete(int id);

}
