package com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts;

import com.telerikacademy.smartgaragegoodoldbilly.models.User;
import com.telerikacademy.smartgaragegoodoldbilly.models.enums.UserSortOptions;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


public interface UserRepository{

    List<User> search(Optional<String> search);

    List<User> filter(Optional<Date> dateFrom, Optional<Date> dateTo,
                      Optional<UserSortOptions> sortOptions);

    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    User getByEmail(String email);

    User create(User user);

    User update(User user);

    void delete(int id);

    void enableAppUser(String email);

}