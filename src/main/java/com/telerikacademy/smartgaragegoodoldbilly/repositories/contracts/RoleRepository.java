package com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts;

import com.telerikacademy.smartgaragegoodoldbilly.models.Role;

import java.util.List;

public interface RoleRepository {

    List<Role> getAll();

    Role getById(int id);

    Role getByRole(String role);

    Role create(Role role);

    Role update(Role role);

    void delete(int id);

}
