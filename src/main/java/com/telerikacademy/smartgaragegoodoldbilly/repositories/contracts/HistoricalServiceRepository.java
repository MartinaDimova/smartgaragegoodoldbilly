package com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts;

import com.telerikacademy.smartgaragegoodoldbilly.models.HistoricalService;

public interface HistoricalServiceRepository {

    HistoricalService getById(int id);

    HistoricalService getByService(String service);

    HistoricalService create(HistoricalService service);

    HistoricalService update(HistoricalService service);

    void delete(int id);

}
