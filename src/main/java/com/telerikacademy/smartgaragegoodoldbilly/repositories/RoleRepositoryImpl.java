package com.telerikacademy.smartgaragegoodoldbilly.repositories;

import com.telerikacademy.smartgaragegoodoldbilly.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgaragegoodoldbilly.models.Role;
import com.telerikacademy.smartgaragegoodoldbilly.repositories.contracts.RoleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleRepositoryImpl implements RoleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Role> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery("from Role ", Role.class);
            return query.list();
        }
    }

    @Override
    public Role getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Role role = session.get(Role.class, id);
            if (role == null) {
                throw new EntityNotFoundException("Role", id);
            }
            return role;
        }
    }

    @Override
    public Role getByRole(String role) {
        try (Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery("from Role where role = :role", Role.class);
            query.setParameter("role", role);
            List<Role> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Role", "role", role);
            }
            return result.get(0);
        }
    }

    @Override
    public Role create(Role role) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(role);
            session.getTransaction().commit();
        }
        return role;
    }

    @Override
    public Role update(Role role) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(role);
            session.getTransaction().commit();
        }
        return role;
    }

    @Override
    public void delete(int id) {
        Role role = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(role);
            session.getTransaction().commit();
        }
    }
}
