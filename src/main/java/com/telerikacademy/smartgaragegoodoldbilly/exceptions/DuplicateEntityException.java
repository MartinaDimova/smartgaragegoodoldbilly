package com.telerikacademy.smartgaragegoodoldbilly.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exists.", type, attribute, value));
    }

    public DuplicateEntityException(String username) {
        super(String.format("Post created by %s already exists.", username));
    }

    public DuplicateEntityException() {
        super("Record already exists.");
    }

}