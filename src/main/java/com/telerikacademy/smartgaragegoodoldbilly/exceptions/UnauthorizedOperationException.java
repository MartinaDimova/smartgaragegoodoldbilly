package com.telerikacademy.smartgaragegoodoldbilly.exceptions;

public class UnauthorizedOperationException extends RuntimeException {

    public static final String NO_AUTH = "The requested resource requires authentication.";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";
    public static final String NO_USER = "No user logged in.";
    public static final String NO_SERVICE = "No working service at the moment. Please try again later.";
    public static final String TOO_LATE = "You can not add or remove service to/from old visit. Please create new one.";

    public UnauthorizedOperationException(String message) {
        super(message);
    }
}
