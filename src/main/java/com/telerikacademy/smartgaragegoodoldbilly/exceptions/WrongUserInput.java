package com.telerikacademy.smartgaragegoodoldbilly.exceptions;

public class WrongUserInput extends RuntimeException {

    public static final String WRONG_PASS = "Invalid user input. Password should contain" +
            "a capital letter, digit, and special symbol (+, -, *, &, ^, …)";

    public static final String WRONG_LICENSE_PLATE = "Invalid user input. " +
            "Please enter valid Bulgarian license plate.";

    public static final String PASS_NOT_MATCH = "Re-entered password doesn't match. Please retype password.";

    public static final String YEAR_NOT_MATCH = "Invalid user input. Year of creation should be whole number larger than 1886.";

    public WrongUserInput(String message) {
        super(message);
    }

}