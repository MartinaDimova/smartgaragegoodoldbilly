package com.telerikacademy.smartgaragegoodoldbilly.exceptions;

public class NotAllowedOperation extends RuntimeException {

    public static final String NOT_EMPTY_OBJECT = "Following operation is not allowed." +
            "There are entities associated with this object.";

    public static final String NOT_ALLOWED = "Following operation is not allowed.";

    public NotAllowedOperation(String message) {
        super(message);
    }
}
