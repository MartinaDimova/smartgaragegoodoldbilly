package com.telerikacademy.smartgaragegoodoldbilly.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserDto {

    @NotEmpty(message = "Username can't be empty.")
    @Size(min = 2, max = 20, message = "Username should be between 2 and 20 symbols.")
    private String username;

    @JsonIgnore
    @NotEmpty(message = "Password can't be empty.")
    @Size(min = 8, message = "Password should be at least 8 symbols.")
    private String password;

    @JsonIgnore
    @NotEmpty(message = "Password confirmation can't be empty.")
    @Size(min = 8, message = "Password should be at least 8 symbols.")
    private String passwordConfirm;

    @NotEmpty(message = "E-mail can't be empty.")
    @Email(message = "Please enter valid e-mail address.", regexp = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$")
    private String email;

    @NotEmpty(message = "Telephone number can't be empty.")
    @Size(min = 10, max = 10, message = "Telephone number should be 10 symbols.")
    private String phone_number;

    @NotNull(message = "RoleId should be chosen.")
    private int roleId;

}
