package com.telerikacademy.smartgaragegoodoldbilly.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Positive;
import java.sql.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VisitDto {

    private Date visit_date;

    @Positive
    private int vehicle_id;
}
