package com.telerikacademy.smartgaragegoodoldbilly.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EmailDto {

    @NotEmpty
    private String email;

}
