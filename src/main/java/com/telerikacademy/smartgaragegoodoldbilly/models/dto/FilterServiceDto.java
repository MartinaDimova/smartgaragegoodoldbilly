package com.telerikacademy.smartgaragegoodoldbilly.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FilterServiceDto {

    private String price;

    private String sortOptions;

}
