package com.telerikacademy.smartgaragegoodoldbilly.models.dto;

import com.telerikacademy.smartgaragegoodoldbilly.models.HistoricalService;
import com.telerikacademy.smartgaragegoodoldbilly.models.User;
import com.telerikacademy.smartgaragegoodoldbilly.models.Vehicle;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VisitDtoOut {

    private int id;

    private Date date;

    private double total_cost;

    private String currency = "BGN";

    private User user;

    private Vehicle vehicle;

    private Set<HistoricalService> hVisitServices;

}