package com.telerikacademy.smartgaragegoodoldbilly.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ModelDto {

    @Size(min = 2, max = 32, message = "Model should be between 2 and 32 symbols.")
    @Column(name = "model")
    private String model;

    @Positive(message = "Brand ID should be positive number.")
    private int brand_id;

}
