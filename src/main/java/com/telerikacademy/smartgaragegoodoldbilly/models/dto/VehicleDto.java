package com.telerikacademy.smartgaragegoodoldbilly.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VehicleDto {

    @Size(min = 7, max = 8, message = "License plate should be from 7 to 8 symbols.")
    private String license_plate;

    @Size(min = 17, max = 17, message = "Vin should be 17 symbols.")
    private String vin;

    @Positive
    private int year;

    @Positive
    private int model_id;

    @Positive
    private int user_id;

}