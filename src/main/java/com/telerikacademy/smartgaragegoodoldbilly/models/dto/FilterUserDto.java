package com.telerikacademy.smartgaragegoodoldbilly.models.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;

@NoArgsConstructor
@Getter
@Setter
public class FilterUserDto {

    private Date dateFrom = Date.valueOf(LocalDate.EPOCH);

    private Date dateTo = Date.valueOf(LocalDate.now());

    private String sortOptions;

    public FilterUserDto(Date dateFrom, Date dateTo, String sortOptions) {
        this.dateFrom = convertToSqlDate(dateFrom);
        this.dateTo = convertToSqlDate(dateTo);
        this.sortOptions = sortOptions;
    }

    public static java.sql.Date convertToSqlDate(java.util.Date date) {
        if (date == null) {
            throw new IllegalArgumentException("date must not be null");
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        java.sql.Date sqlDate = new java.sql.Date(cal.getTimeInMillis());
        return sqlDate;
    }
}