package com.telerikacademy.smartgaragegoodoldbilly.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum VehicleSortOptions {

    BRAND_ASC("Brand, ascending", " order by b.brand "),
    BRAND_DESC("Brand, descending", " order by b.brand desc "),
    MODEL_ASC("Model, ascending", " order by m.model "),
    MODEL_DESC("Model, descending", " order by m.model desc "),
    YEAR_ASC("Year, ascending", " order by v.year "),
    YEAR_DESC("Year, descending", " order by v.year desc ");

    private final String preview;
    private final String query;

    private static final Map<String, VehicleSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (VehicleSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    VehicleSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static VehicleSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}