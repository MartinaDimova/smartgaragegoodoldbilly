package com.telerikacademy.smartgaragegoodoldbilly.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum ServiceSortOptions {

    NAME_ASC("Name, ascending", "order by service"),
    NAME_DESC("Name, descending", "order by service desc");

    private final String preview;
    private final String query;

    private static final Map<String, ServiceSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (ServiceSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    ServiceSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static ServiceSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}