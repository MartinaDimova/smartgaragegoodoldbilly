package com.telerikacademy.smartgaragegoodoldbilly.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum ServicePriceRangeOptions {

    LESS_THAN_100("Less than 100", " price between 0 and 100 "),
    BETWEEN_101_AND_300("Between 101 and 300", " price between 101 and 300 "),
    BETWEEN_301_AND_500("Between 301 and 500", " price between 301 and 500 "),
    BETWEEN_501_AND_700("Between 501 and 700", " price between 501 and 700 "),
    BETWEEN_701_AND_999("Between 701 and 999", " price between 701 and 999 "),
    ABOVE_1000("Above 1000", " price > 1000 ");

    private final String preview;
    private final String query;

    private static final Map<String, ServicePriceRangeOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (ServicePriceRangeOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    ServicePriceRangeOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static ServicePriceRangeOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}