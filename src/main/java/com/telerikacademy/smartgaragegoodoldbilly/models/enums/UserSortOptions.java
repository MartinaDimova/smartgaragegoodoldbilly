package com.telerikacademy.smartgaragegoodoldbilly.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum UserSortOptions {

    NAME_ASC("Name, ascending", " order by u.username"),
    NAME_DESC("Name, descending", " order by u.username desc"),
    LATEST("Latest visits", " order by v.visit_date desc"),
    FIRST("Visits firsts", " order by v.visit_date");

    private final String preview;
    private final String query;

    private static final Map<String, UserSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (UserSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    UserSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static UserSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}