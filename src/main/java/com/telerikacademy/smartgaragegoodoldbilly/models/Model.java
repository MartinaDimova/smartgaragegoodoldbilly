package com.telerikacademy.smartgaragegoodoldbilly.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "models")
public class Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "model_id")
    private int id;

    @Column(name = "model")
    private String model;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "brand_id")
    private Brand brand;

    @JsonIgnore
    @OneToMany(mappedBy = "model", fetch = FetchType.EAGER)
    private Set<Vehicle> modelVehicles = new HashSet<>();

    public Model(String model) {
        id++;
        this.model = model;
    }

    public void addVehicle(Vehicle vehicle) {
        modelVehicles.add(vehicle);
    }

    public void removeVehicle(Vehicle vehicle) {
        modelVehicles.remove(vehicle);
    }

}
