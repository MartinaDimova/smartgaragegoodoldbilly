package com.telerikacademy.smartgaragegoodoldbilly.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.WrongUserInput;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Column(name = "username")
    private String username;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phone_number;

    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Set<Vehicle> userVehicles = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Set<Visit> userVisits = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> userRoles = new HashSet<>();

    @Transient
    private Boolean locked = false;

    @Column(name = "enabled")
    private Boolean enabled = false;

    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Set<ConfirmationToken> userTokens = new HashSet<>();

    public User(String username, String password, String email, String phone_number) {
        id++;
        this.username = username;
        setPassword(password);
        this.email = email;
        this.phone_number = phone_number;
    }

    public void addVehicle(Vehicle vehicle) {
        userVehicles.add(vehicle);
    }

    public void removeVehicle(Vehicle vehicle) {
        userVehicles.remove(vehicle);
    }

    public void addVisit(Visit visit) {
        userVisits.add(visit);
    }

    public void removeVisit(Visit visit) {
        userVisits.remove(visit);
    }

    public void addUserRole(Role role) {
        userRoles.add(role);
    }

    public void removeUserRole(Role role) {
        userRoles.remove(role);
    }

    public void setPassword(String password) {
        Pattern regex = Pattern.compile("[^a-z]");
        Matcher matcher = regex.matcher(password);
        if (!matcher.find()) {
            throw new WrongUserInput(WrongUserInput.WRONG_PASS);
        }
        this.password = password;
    }
}