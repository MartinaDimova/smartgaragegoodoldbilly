package com.telerikacademy.smartgaragegoodoldbilly.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "h_services")
public class HistoricalService {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "h_service_id")
    private int id;

    @NotNull
    @Column(name = "service")
    private String service;

    @Positive
    @Column(name = "price")
    private Double price;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(
            name = "visits_h_services",
            joinColumns = @JoinColumn(name = "h_service_id"),
            inverseJoinColumns = @JoinColumn(name = "visit_id")
    )
    private Set<Visit> hServiceVisits = new HashSet<>();

    public HistoricalService(String service, Double price) {
        id++;
        this.service = service;
        this.price = price;
    }

}

