package com.telerikacademy.smartgaragegoodoldbilly.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private int id;

    @Size(min = 2, max = 30, message = "Role should be between 2 and 30 symbols.")
    @Column(name = "role")
    private String role;

    @JsonIgnore
    @ManyToMany(mappedBy = "userRoles", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Set<User> roleUsers = new HashSet<>();

    public Role(int id, String role) {
        this.id = id;
        this.role = role;
    }

    public void addRoleUsers(User user) {
        roleUsers.add(user);
    }

    public void removeRoleUsers(User user) {
        roleUsers.remove(user);
    }
}
