package com.telerikacademy.smartgaragegoodoldbilly.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "services")
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_id")
    private int id;

    @NotNull
    @Column(name = "service")
    private String service;

    @Positive
    @Column(name = "price")
    private Double price;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(
            name = "visits_services",
            joinColumns = @JoinColumn(name = "service_id"),
            inverseJoinColumns = @JoinColumn(name = "visit_id")
    )
    private Set<Visit> serviceVisits = new HashSet<>();

    public Service(String service, Double price) {
        id++;
        this.service = service;
        this.price = price;
    }

    public void addVisit(Visit visit) {
        serviceVisits.add(visit);
    }

    public void removeVisit(Visit visit) {
        serviceVisits.remove(visit);
    }

}
