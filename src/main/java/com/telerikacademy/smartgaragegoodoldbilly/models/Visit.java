package com.telerikacademy.smartgaragegoodoldbilly.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "visits")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "visit_id")
    private int id;

    @Column(name = "visit_date")
    private Date date;

    @Column(name = "total_cost")
    private Double total_cost;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany(mappedBy = "serviceVisits", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Set<Service> visitServices = new HashSet<>();

    @ManyToMany(mappedBy = "hServiceVisits", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Set<HistoricalService> hVisitServices = new HashSet<>();

    public Visit(Date date, Vehicle vehicle) {
        id++;
        this.date = date;
        this.vehicle = vehicle;
        setTotal_cost();
    }

    public void setTotal_cost() {
        double total = 0;
        for (HistoricalService service : getHVisitServices()) {
            total = total + service.getPrice();
        }
        this.total_cost = total;
    }

    public void sethVisitServices(Set<Service> serviceVisits) {
        for (Service service : serviceVisits) {
            HistoricalService historicalService = new HistoricalService();
            historicalService.setService(service.getService());
            historicalService.setPrice(service.getPrice());
            hVisitServices.add(historicalService);
        }
    }
}
