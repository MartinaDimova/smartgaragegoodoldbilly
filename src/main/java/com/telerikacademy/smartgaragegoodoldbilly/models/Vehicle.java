package com.telerikacademy.smartgaragegoodoldbilly.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.smartgaragegoodoldbilly.exceptions.WrongUserInput;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "vehicles")
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vehicle_id")
    private int id;

    @Column(name = "license_plate")
    private String license_plate;

    @Column(name = "vin")
    private String vin;

    @Column(name = "year")
    private int year;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "model_id")
    private Model model;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id")
    private User user;

    @JsonIgnore
    @OneToMany(mappedBy = "vehicle", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Set<Visit> vehicleVisits = new HashSet<>();

    public Vehicle(String license_plate, String vin, int year, Model model, User user) {
        id++;
        setLicense_plate(license_plate);
        this.vin = vin;
        setYear(year);
        this.model = model;
        this.user = user;
    }

    public void setLicense_plate(String license_plate) {
        if (license_plate.matches("[A-Z0-9 ]+")) {
            this.license_plate = license_plate;
        } else {
            throw new WrongUserInput(WrongUserInput.WRONG_LICENSE_PLATE);
        }
    }

    public void setYear(int year) {
        if (year < 1886 || year > LocalDate.now().getYear()) {
            throw new WrongUserInput(WrongUserInput.YEAR_NOT_MATCH);
        }
        this.year = year;
    }

    public void addVehicleVisit(Visit visit) {
        vehicleVisits.add(visit);
    }

    public void removeVehicleVisit(Visit visit) {
        vehicleVisits.remove(visit);
    }

}