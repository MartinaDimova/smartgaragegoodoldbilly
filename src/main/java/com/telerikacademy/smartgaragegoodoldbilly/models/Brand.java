package com.telerikacademy.smartgaragegoodoldbilly.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "brands")
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "brand_id")
    private int id;

    @Size(min = 2, max = 32, message = "Brand should be between 2 and 32 symbols.")
    @Column(name = "brand")
    private String brand;

    @JsonIgnore
    @OneToMany(mappedBy = "brand", fetch = FetchType.EAGER)
    private Set<Model> brandModels = new HashSet<>();

    public Brand(String brand) {
        id++;
        this.brand = brand;
    }

    public void addModel(Model model) {
        brandModels.add(model);
    }

    public void removeModel(Model model) {
        brandModels.remove(model);
    }

}
