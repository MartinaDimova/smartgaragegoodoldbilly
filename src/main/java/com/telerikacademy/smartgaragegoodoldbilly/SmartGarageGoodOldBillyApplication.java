package com.telerikacademy.smartgaragegoodoldbilly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartGarageGoodOldBillyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmartGarageGoodOldBillyApplication.class, args);
    }

}
