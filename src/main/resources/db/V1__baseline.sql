/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;


-- Dumping database structure for smartgarage
CREATE DATABASE IF NOT EXISTS `smartgarage` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smartgarage`;

-- Dumping structure for table smartgarage.brands
CREATE TABLE IF NOT EXISTS `brands`
(
    `brand_id` int(11)     NOT NULL AUTO_INCREMENT,
    `brand`    varchar(32) NOT NULL,
    PRIMARY KEY (`brand_id`),
    UNIQUE KEY `brands_brand_id_uindex` (`brand_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 124
  DEFAULT CHARSET = latin1;

-- Dumping data for table smartgarage.brands: ~46 rows (approximately)
/*!40000 ALTER TABLE `brands`
    DISABLE KEYS */;
INSERT INTO `brands` (`brand_id`, `brand`)
VALUES (1, 'Acura'),
       (2, 'Alfa Romeo'),
       (80, 'Aston Martin'),
       (81, 'Audi'),
       (82, 'Bentley'),
       (83, 'BMW'),
       (84, 'Buick'),
       (85, 'Cadillac'),
       (86, 'Chevrolet'),
       (87, 'Chrysler'),
       (88, 'Dodge'),
       (89, 'Ferrari'),
       (90, 'FIAT'),
       (91, 'Ford'),
       (92, 'Freightliner'),
       (93, 'Genesis'),
       (94, 'GMC'),
       (95, 'Honda'),
       (96, 'Hyundai'),
       (97, 'INFINITI'),
       (98, 'Jaguar'),
       (99, 'Jeep'),
       (100, 'Kia'),
       (101, 'Lamborghini'),
       (102, 'Land Rover'),
       (103, 'Lexus'),
       (104, 'Lincoln'),
       (105, 'Lotus'),
       (106, 'Lucid'),
       (107, 'Maserati'),
       (108, 'MAZDA'),
       (109, 'Mercedes-Benz'),
       (110, 'MINI'),
       (111, 'Mitsubishi'),
       (112, 'Nissan'),
       (113, 'Polestar'),
       (114, 'Porsche'),
       (115, 'Ram'),
       (116, 'Rivian'),
       (117, 'Rolls-Royce'),
       (118, 'Subaru'),
       (119, 'Tesla'),
       (120, 'Toyota'),
       (121, 'VinFast'),
       (122, 'Volkswagen'),
       (123, 'Volvo');
/*!40000 ALTER TABLE `brands`
    ENABLE KEYS */;

-- Dumping structure for table smartgarage.confirmation_tokens
CREATE TABLE IF NOT EXISTS `confirmation_tokens`
(
    `confirmation_token_id` int(11) NOT NULL AUTO_INCREMENT,
    `token`                 varchar(100) DEFAULT NULL,
    `createdAt`             datetime     DEFAULT NULL,
    `expiresAt`             datetime     DEFAULT NULL,
    `confirmedAt`           datetime     DEFAULT NULL,
    `user_id`               int(11)      DEFAULT NULL,
    PRIMARY KEY (`confirmation_token_id`),
    UNIQUE KEY `confirmation_tokens_confirmation_token_id_uindex` (`confirmation_token_id`),
    KEY `confirmation_tokens_users_user_id_fk` (`user_id`),
    CONSTRAINT `confirmation_tokens_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE = InnoDB
  AUTO_INCREMENT = 20
  DEFAULT CHARSET = latin1;

-- Dumping data for table smartgarage.confirmation_tokens: ~11 rows (approximately)
/*!40000 ALTER TABLE `confirmation_tokens`
    DISABLE KEYS */;
INSERT INTO `confirmation_tokens` (`confirmation_token_id`, `token`, `createdAt`, `expiresAt`, `confirmedAt`, `user_id`)
VALUES (2, 'c2396215-cee4-4854-bdb7-4dab78cd79f2', '2022-04-09 15:36:44', '2022-04-10 15:36:44', NULL, 33),
       (3, '3f8419eb-f7c6-4ab1-9e64-27fe14ae8ee1', '2022-04-09 15:44:26', '2022-04-10 15:44:26', NULL, 35),
       (4, 'd02c06ff-585f-48b1-abf8-c3258eb16415', '2022-04-13 14:48:31', '2022-04-14 14:48:31', NULL, 36),
       (5, '8aa67337-b280-4271-91a6-1692c5da4ca3', '2022-04-13 16:25:17', '2022-04-14 16:25:17', NULL, 37),
       (6, 'dd7ee34f-8e1c-4105-915f-c187102a09e2', '2022-04-13 16:36:24', '2022-04-14 16:36:24', NULL, 38),
       (7, 'dd14e180-f2e3-41d5-bdf4-837b4dda52f1', '2022-04-13 17:11:05', '2022-04-14 17:11:05', '2022-04-13 17:11:38',
        39),
       (8, '621b695c-f7cd-4f5b-9feb-56a402a63cdb', '2022-04-13 17:20:00', '2022-04-14 17:20:00', '2022-04-13 17:20:13',
        40),
       (13, '9f773a00-374c-4b9a-8d03-28366a0844dc', '2022-04-22 17:51:21', '2022-04-23 17:51:21', '2022-04-22 17:51:28',
        45),
       (17, '6d626455-ca4d-4402-9907-22cbb6198f77', '2022-04-28 10:20:37', '2022-04-29 10:20:37', '2022-04-28 10:20:46',
        49),
       (18, '6277eaab-57f3-44c5-ac09-e92d4d141c42', '2022-04-28 11:06:50', '2022-04-29 11:06:50', '2022-04-28 11:07:01',
        50),
       (19, '375caec8-cfa6-4c62-8fcf-a051a48b6ccc', '2022-05-01 09:07:58', '2022-05-02 09:07:58', '2022-05-01 09:09:17',
        51);
/*!40000 ALTER TABLE `confirmation_tokens`
    ENABLE KEYS */;

-- Dumping structure for table smartgarage.h_services
CREATE TABLE IF NOT EXISTS `h_services`
(
    `h_service_id` int(11)      NOT NULL AUTO_INCREMENT,
    `service`      varchar(100) NOT NULL,
    `price`        double       NOT NULL,
    PRIMARY KEY (`h_service_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 12
  DEFAULT CHARSET = latin1;

-- Dumping data for table smartgarage.h_services: ~11 rows (approximately)
/*!40000 ALTER TABLE `h_services`
    DISABLE KEYS */;
INSERT INTO `h_services` (`h_service_id`, `service`, `price`)
VALUES (1, 'oil change High', 325),
       (2, 'oil change Low', 120),
       (3, 'Timing belt change High', 1000),
       (4, 'Water pump change for Low', 400.5),
       (5, 'Tire change for High', 40.96),
       (6, 'Rear view mirror fix', 58),
       (7, 'A/C Repair Low', 2500),
       (8, 'A/C Repair High', 6500),
       (9, 'Timing belt change High', 750),
       (10, 'Tire change for Low', 25),
       (11, 'Paintjob whole car', 5000);
/*!40000 ALTER TABLE `h_services`
    ENABLE KEYS */;

-- Dumping structure for table smartgarage.models
CREATE TABLE IF NOT EXISTS `models`
(
    `model_id` int(11)     NOT NULL AUTO_INCREMENT,
    `model`    varchar(32) NOT NULL,
    `brand_id` int(11)     NOT NULL,
    PRIMARY KEY (`model_id`),
    UNIQUE KEY `models_model_id_uindex` (`model_id`),
    KEY `models_brands_brand_id_fk` (`brand_id`),
    CONSTRAINT `models_brands_brand_id_fk` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 550
  DEFAULT CHARSET = latin1;

-- Dumping data for table smartgarage.models: ~535 rows (approximately)
/*!40000 ALTER TABLE `models`
    DISABLE KEYS */;
INSERT INTO `models` (`model_id`, `model`, `brand_id`)
VALUES (1, 'ILX', 1),
       (2, 'Integra', 1),
       (5, 'MDX', 1),
       (6, 'NSX', 1),
       (7, 'RDX', 1),
       (8, 'TLX', 1),
       (9, 'Giulia', 2),
       (10, 'Stelvio', 2),
       (11, 'Tonale', 2),
       (12, 'DB11', 80),
       (13, 'DBS Superleggera', 80),
       (14, 'DBX', 80),
       (15, 'Vantage', 80),
       (16, 'A3', 81),
       (17, 'A4', 81),
       (18, 'A4 allroad', 81),
       (19, 'A5', 81),
       (20, 'A6', 81),
       (21, 'A6 allroad', 81),
       (22, 'A7', 81),
       (23, 'A8', 81),
       (24, 'e-tron', 81),
       (25, 'e-tron GT', 81),
       (26, 'e-tron S', 81),
       (27, 'e-tron S Sportback', 81),
       (28, 'e-tron Sportback', 81),
       (29, 'Q3', 81),
       (30, 'Q4 e-tron', 81),
       (31, 'Q4 Sportback e-tron', 81),
       (32, 'Q5', 81),
       (33, 'Q5 Sportback', 81),
       (34, 'Q7', 81),
       (35, 'Q8', 81),
       (36, 'R8', 81),
       (37, 'RS 5', 81),
       (38, 'RS 6', 81),
       (39, 'RS 7', 81),
       (40, 'RS e-tron GT', 81),
       (41, 'RS Q8', 81),
       (42, 'S3', 81),
       (43, 'S4', 81),
       (44, 'S5', 81),
       (45, 'S6', 81),
       (46, 'S7', 81),
       (47, 'S8', 81),
       (48, 'SQ5', 81),
       (49, 'SQ5 Sportback', 81),
       (50, 'SQ7', 81),
       (51, 'SQ8', 81),
       (52, 'TT', 81),
       (53, 'Bentayga', 82),
       (54, 'Continental GT', 82),
       (55, 'Flying Spur', 82),
       (56, 'i3', 83),
       (57, 'i4', 83),
       (58, 'iX', 83),
       (59, 'M2', 83),
       (60, 'M3', 83),
       (61, 'M4', 83),
       (62, 'M5', 83),
       (63, 'M8', 83),
       (64, 'X1', 83),
       (65, 'X2', 83),
       (66, 'X3', 83),
       (67, 'X3 M', 83),
       (68, 'X4', 83),
       (69, 'X4 M', 83),
       (70, 'X5', 83),
       (71, 'X5 M', 83),
       (72, 'X6', 83),
       (73, 'X6 M', 83),
       (74, 'X7', 83),
       (75, 'Z4', 83),
       (76, '2 Series', 83),
       (77, '3 Series', 83),
       (78, '4 Series', 83),
       (79, '5 Series', 83),
       (80, '7 Series', 83),
       (81, '8 Series', 83),
       (82, 'Enclave', 84),
       (83, 'Encore', 84),
       (84, 'Encore GX', 84),
       (85, 'Envision', 84),
       (86, 'CT4', 85),
       (87, 'CT5', 85),
       (88, 'Escalade', 85),
       (89, 'Escalade ESV', 85),
       (90, 'LYRIQ', 85),
       (91, 'XT4', 85),
       (92, 'XT5', 85),
       (93, 'XT6', 85),
       (94, 'Blazer', 86),
       (95, 'Blazer EV', 86),
       (96, 'Bolt EUV', 86),
       (97, 'Bolt EV', 86),
       (98, 'Camaro', 86),
       (99, 'Colorado Crew Cab', 86),
       (100, 'Colorado Extended Cab', 86),
       (101, 'Corvette', 86),
       (102, 'Equinox', 86),
       (103, 'Equinox EV', 86),
       (104, 'Express 2500 Cargo', 86),
       (105, 'Express 2500 Passenger', 86),
       (106, 'Express 3500 Cargo', 86),
       (107, 'Express 3500 Passenger', 86),
       (108, 'Malibu', 86),
       (109, 'Silverado 1500 Crew Cab', 86),
       (110, 'Silverado 1500 Double Cab', 86),
       (111, 'Silverado 1500 Limited Crew Cab', 86),
       (112, 'Silverado 1500 Limited Double Ca', 86),
       (113, 'Silverado 1500 Limited Regular C', 86),
       (114, 'Silverado 1500 Regular Cab', 86),
       (115, 'Silverado 2500 HD Crew Cab', 86),
       (116, 'Silverado 2500 HD Double Cab', 86),
       (117, 'Silverado 2500 HD Regular Cab', 86),
       (118, 'Silverado 3500 HD Crew Cab', 86),
       (119, 'Silverado 3500 HD Double Cab', 86),
       (120, 'Silverado 3500 HD Regular Cab', 86),
       (121, 'Silverado EV', 86),
       (122, 'Spark', 86),
       (123, 'Suburban', 86),
       (124, 'Tahoe', 86),
       (125, 'Trailblazer', 86),
       (126, 'Traverse', 86),
       (127, 'Trax', 86),
       (128, 'Pacifica', 87),
       (129, 'Pacifica Hybrid', 87),
       (130, 'Voyager', 87),
       (131, '300', 87),
       (132, 'Challenger', 88),
       (133, 'Charger', 88),
       (134, 'Durango', 88),
       (135, 'F8', 89),
       (136, 'Portofino', 89),
       (137, 'Roma', 89),
       (138, 'SF90', 89),
       (139, '488 Pista', 89),
       (140, '812 GTS', 89),
       (141, '812 Superfast', 89),
       (142, '500X', 90),
       (143, 'Bronco', 91),
       (144, 'Bronco Sport', 91),
       (145, 'E-Transit 350 Cargo Van', 91),
       (146, 'EcoSport', 91),
       (147, 'Edge', 91),
       (148, 'Escape', 91),
       (149, 'Expedition', 91),
       (150, 'Expedition MAX', 91),
       (151, 'Explorer', 91),
       (152, 'F150 Lightning', 91),
       (153, 'F150 Regular Cab', 91),
       (154, 'F150 Super Cab', 91),
       (155, 'F150 SuperCrew Cab', 91),
       (156, 'F250 Super Duty Crew Cab', 91),
       (157, 'F250 Super Duty Regular Cab', 91),
       (158, 'F250 Super Duty Super Cab', 91),
       (159, 'F350 Super Duty Crew Cab', 91),
       (160, 'F350 Super Duty Regular Cab', 91),
       (161, 'F350 Super Duty Super Cab', 91),
       (162, 'F450 Super Duty Crew Cab', 91),
       (163, 'F450 Super Duty Regular Cab', 91),
       (164, 'Maverick', 91),
       (165, 'Mustang', 91),
       (166, 'Mustang MACH-E', 91),
       (167, 'Ranger SuperCab', 91),
       (168, 'Ranger SuperCrew', 91),
       (169, 'Transit 150 Cargo Van', 91),
       (170, 'Transit 150 Crew Van', 91),
       (171, 'Transit 150 Passenger Van', 91),
       (172, 'Transit 250 Cargo Van', 91),
       (173, 'Transit 250 Crew Van', 91),
       (174, 'Transit 350 Cargo Van', 91),
       (175, 'Transit 350 Crew Van', 91),
       (176, 'Transit 350 HD Cargo Van', 91),
       (177, 'Transit 350 HD Crew Van', 91),
       (178, 'Transit 350 Passenger Van', 91),
       (179, 'Transit Connect Cargo Van', 91),
       (180, 'Transit Connect Passenger Wagon', 91),
       (181, 'Sprinter 1500 Cargo', 92),
       (182, 'Sprinter 1500 Passenger', 92),
       (183, 'Sprinter 2500 Cargo', 92),
       (184, 'Sprinter 2500 Crew', 92),
       (185, 'Sprinter 2500 Passenger', 92),
       (186, 'Sprinter 3500 Cargo', 92),
       (187, 'Sprinter 3500 Crew', 92),
       (188, 'Sprinter 3500 XD Crew', 92),
       (189, 'Sprinter 3500XD Cargo', 92),
       (190, 'Sprinter 4500 Cargo', 92),
       (191, 'Sprinter 4500 Crew', 92),
       (192, 'Electrified G80', 93),
       (193, 'G70', 93),
       (194, 'G80', 93),
       (195, 'G90', 93),
       (196, 'GV60', 93),
       (197, 'GV70', 93),
       (198, 'GV80', 93),
       (199, 'Acadia', 94),
       (200, 'Canyon Crew Cab', 94),
       (201, 'Canyon Extended Cab', 94),
       (202, 'Hummer EV', 94),
       (203, 'Hummer EV SUV', 94),
       (204, 'Savana 2500 Cargo', 94),
       (205, 'Savana 2500 Passenger', 94),
       (206, 'Savana 3500 Cargo', 94),
       (207, 'Savana 3500 Passenger', 94),
       (208, 'Sierra 1500 Crew Cab', 94),
       (209, 'Sierra 1500 Double Cab', 94),
       (210, 'Sierra 1500 Limited Crew Cab', 94),
       (211, 'Sierra 1500 Limited Double Cab', 94),
       (212, 'Sierra 1500 Limited Regular Cab', 94),
       (213, 'Sierra 1500 Regular Cab', 94),
       (214, 'Sierra 2500 HD Crew Cab', 94),
       (215, 'Sierra 2500 HD Double Cab', 94),
       (216, 'Sierra 2500 HD Regular Cab', 94),
       (217, 'Sierra 3500 HD Crew Cab', 94),
       (218, 'Sierra 3500 HD Double Cab', 94),
       (219, 'Sierra 3500 HD Regular Cab', 94),
       (220, 'Terrain', 94),
       (221, 'Yukon', 94),
       (222, 'Yukon XL', 94),
       (223, 'Accord', 95),
       (224, 'Accord Hybrid', 95),
       (225, 'Civic', 95),
       (226, 'Civic Type R', 95),
       (227, 'Clarity Fuel Cell', 95),
       (228, 'Clarity Plug-in Hybrid', 95),
       (229, 'CR-V', 95),
       (230, 'CR-V Hybrid', 95),
       (231, 'HR-V', 95),
       (232, 'Insight', 95),
       (233, 'Odyssey', 95),
       (234, 'Passport', 95),
       (235, 'Pilot', 95),
       (236, 'Prologue', 95),
       (237, 'Ridgeline', 95),
       (238, 'Accent', 96),
       (239, 'Elantra', 96),
       (240, 'Elantra Hybrid', 96),
       (241, 'Elantra N', 96),
       (242, 'IONIQ 5', 96),
       (243, 'Ioniq Electric', 96),
       (244, 'Ioniq Hybrid', 96),
       (245, 'Ioniq Plug-in Hybrid', 96),
       (246, 'Kona', 96),
       (247, 'Kona Electric', 96),
       (248, 'Kona N', 96),
       (249, 'NEXO', 96),
       (250, 'Palisade', 96),
       (251, 'Santa Cruz', 96),
       (252, 'Santa Fe', 96),
       (253, 'Santa Fe Hybrid', 96),
       (254, 'Santa Fe Plug-in Hybrid', 96),
       (255, 'Sonata', 96),
       (256, 'Sonata Hybrid', 96),
       (257, 'Tucson', 96),
       (258, 'Tucson Hybrid', 96),
       (259, 'Tucson Plug-in Hybrid', 96),
       (260, 'Veloster', 96),
       (261, 'Venue', 96),
       (262, 'Q50', 97),
       (263, 'Q60', 97),
       (264, 'QX50', 97),
       (265, 'QX55', 97),
       (266, 'QX60', 97),
       (267, 'QX80', 97),
       (268, 'E-PACE', 98),
       (269, 'F-PACE', 98),
       (270, 'F-TYPE', 98),
       (271, 'I-PACE', 98),
       (272, 'XF', 98),
       (273, 'Cherokee', 99),
       (274, 'Compass', 99),
       (275, 'Gladiator', 99),
       (276, 'Grand Cherokee', 99),
       (277, 'Grand Cherokee 4xe', 99),
       (278, 'Grand Cherokee L', 99),
       (279, 'Grand Wagoneer', 99),
       (280, 'Renegade', 99),
       (281, 'Wagoneer', 99),
       (282, 'Wrangler', 99),
       (283, 'Wrangler Unlimited', 99),
       (284, 'Wrangler Unlimited 4xe', 99),
       (285, 'Carnival', 100),
       (286, 'EV6', 100),
       (287, 'Forte', 100),
       (288, 'K5', 100),
       (289, 'Niro', 100),
       (290, 'Niro EV', 100),
       (291, 'Niro Plug-in Hybrid', 100),
       (292, 'Rio', 100),
       (293, 'Sedona', 100),
       (294, 'Seltos', 100),
       (295, 'Sorento', 100),
       (296, 'Sorento Hybrid', 100),
       (297, 'Sorento Plug-in Hybrid', 100),
       (298, 'Soul', 100),
       (299, 'Sportage', 100),
       (300, 'Sportage Hybrid', 100),
       (301, 'Sportage Plug-in Hybrid', 100),
       (302, 'Stinger', 100),
       (303, 'Telluride', 100),
       (304, 'Aventador', 101),
       (305, 'Huracan', 101),
       (306, 'Urus', 101),
       (307, 'Defender 110', 102),
       (308, 'Defender 90', 102),
       (309, 'Discovery', 102),
       (310, 'Discovery Sport', 102),
       (311, 'Range Rover', 102),
       (312, 'Range Rover Evoque', 102),
       (313, 'Range Rover Sport', 102),
       (314, 'Range Rover Velar', 102),
       (315, 'ES', 103),
       (316, 'GX', 103),
       (317, 'IS', 103),
       (318, 'LC', 103),
       (319, 'LS', 103),
       (320, 'LX', 103),
       (321, 'NX', 103),
       (322, 'RC', 103),
       (323, 'RX', 103),
       (324, 'RZ', 103),
       (325, 'UX', 103),
       (326, 'Aviator', 104),
       (327, 'Corsair', 104),
       (328, 'Nautilus', 104),
       (329, 'Navigator', 104),
       (330, 'Navigator L', 104),
       (331, 'Evora GT', 105),
       (332, 'Air', 106),
       (333, 'Ghibli', 107),
       (334, 'Grecale', 107),
       (335, 'Levante', 107),
       (336, 'MC20', 107),
       (337, 'Quattroporte', 107),
       (338, 'CX-3', 108),
       (339, 'CX-30', 108),
       (340, 'CX-5', 108),
       (341, 'CX-50', 108),
       (342, 'CX-70', 108),
       (343, 'CX-9', 108),
       (344, 'CX-90', 108),
       (345, 'MAZDA3', 108),
       (346, 'MAZDA6', 108),
       (347, 'MX-30', 108),
       (348, 'MX-5 Miata', 108),
       (349, 'MX-5 Miata RF', 108),
       (350, 'A-Class', 109),
       (351, 'C-Class', 109),
       (352, 'CLA', 109),
       (353, 'CLS', 109),
       (354, 'E-Class', 109),
       (355, 'G-Class', 109),
       (356, 'GLA', 109),
       (357, 'GLB', 109),
       (358, 'GLC', 109),
       (359, 'GLC Coupe', 109),
       (360, 'GLE', 109),
       (361, 'GLS', 109),
       (362, 'Mercedes-AMG A-Class', 109),
       (363, 'Mercedes-AMG C-Class', 109),
       (364, 'Mercedes-AMG CLA', 109),
       (365, 'Mercedes-AMG CLS', 109),
       (366, 'Mercedes-AMG E-Class', 109),
       (367, 'Mercedes-AMG G-Class', 109),
       (368, 'Mercedes-AMG GLA', 109),
       (369, 'Mercedes-AMG GLB', 109),
       (370, 'Mercedes-AMG GLC', 109),
       (371, 'Mercedes-AMG GLC Coupe', 109),
       (372, 'Mercedes-AMG GLE', 109),
       (373, 'Mercedes-AMG GLE Coupe', 109),
       (374, 'Mercedes-AMG GLS', 109),
       (375, 'Mercedes-AMG GT', 109),
       (376, 'Mercedes-AMG S-Class', 109),
       (377, 'Mercedes-AMG SL', 109),
       (378, 'Mercedes-EQ EQB', 109),
       (379, 'Mercedes-EQ EQE', 109),
       (380, 'Mercedes-EQ EQS', 109),
       (381, 'Mercedes-EQ EQS SUV', 109),
       (382, 'Mercedes-Maybach GLS', 109),
       (383, 'Mercedes-Maybach S-Class', 109),
       (384, 'Metris Cargo', 109),
       (385, 'Metris Passenger', 109),
       (386, 'S-Class', 109),
       (387, 'Sprinter 1500 Cargo', 109),
       (388, 'Sprinter 1500 Passenger', 109),
       (389, 'Sprinter 2500 Cargo', 109),
       (390, 'Sprinter 2500 Crew', 109),
       (391, 'Sprinter 2500 Passenger', 109),
       (392, 'Sprinter 3500 Cargo', 109),
       (393, 'Sprinter 3500 Crew', 109),
       (394, 'Sprinter 3500 XD Cargo', 109),
       (395, 'Sprinter 3500 XD Crew', 109),
       (396, 'Sprinter 4500 Cargo', 109),
       (397, 'Sprinter 4500 Crew', 109),
       (398, 'Clubman', 110),
       (399, 'Convertible', 110),
       (400, 'Countryman', 110),
       (401, 'Hardtop 2 Door', 110),
       (402, 'Hardtop 4 Door', 110),
       (403, 'Eclipse Cross', 111),
       (404, 'Mirage', 111),
       (405, 'Mirage G4', 111),
       (406, 'Outlander', 111),
       (407, 'Outlander PHEV', 111),
       (408, 'Outlander Sport', 111),
       (409, 'Altima', 112),
       (410, 'Ariya', 112),
       (411, 'Armada', 112),
       (412, 'Frontier Crew Cab', 112),
       (413, 'Frontier King Cab', 112),
       (414, 'GT-R', 112),
       (415, 'Kicks', 112),
       (416, 'LEAF', 112),
       (417, 'Maxima', 112),
       (418, 'Murano', 112),
       (419, 'NV1500 Cargo', 112),
       (420, 'NV200', 112),
       (421, 'NV2500 HD Cargo', 112),
       (422, 'NV3500 HD Cargo', 112),
       (423, 'NV3500 HD Passenger', 112),
       (424, 'Pathfinder', 112),
       (425, 'Rogue', 112),
       (426, 'Rogue Sport', 112),
       (427, 'Sentra', 112),
       (428, 'Titan Crew Cab', 112),
       (429, 'Titan King Cab', 112),
       (430, 'TITAN XD Crew Cab', 112),
       (431, 'Versa', 112),
       (432, 'Z', 112),
       (433, '400Z', 112),
       (434, '1', 113),
       (435, '2', 113),
       (436, '5', 113),
       (437, 'Cayenne', 114),
       (438, 'Cayenne Coupe', 114),
       (439, 'Macan', 114),
       (440, 'Panamera', 114),
       (441, 'Taycan', 114),
       (442, 'Taycan Cross Turismo', 114),
       (443, '718 Boxster', 114),
       (444, '718 Cayman', 114),
       (445, '718 Spyder', 114),
       (446, '911', 114),
       (447, 'ProMaster Cargo Van', 115),
       (448, 'ProMaster City', 115),
       (449, 'ProMaster Window Van', 115),
       (450, '1500 Classic Crew Cab', 115),
       (451, '1500 Classic Quad Cab', 115),
       (452, '1500 Classic Regular Cab', 115),
       (453, '1500 Crew Cab', 115),
       (454, '1500 Quad Cab', 115),
       (455, '2500 Crew Cab', 115),
       (456, '2500 Mega Cab', 115),
       (457, '2500 Regular Cab', 115),
       (458, '3500 Crew Cab', 115),
       (459, '3500 Mega Cab', 115),
       (460, '3500 Regular Cab', 115),
       (461, 'R1S', 116),
       (462, 'R1T', 116),
       (463, 'Cullinan', 117),
       (464, 'Dawn', 117),
       (465, 'Ghost', 117),
       (466, 'Phantom', 117),
       (467, 'Wraith', 117),
       (468, 'Ascent', 118),
       (469, 'BRZ', 118),
       (470, 'Crosstrek', 118),
       (471, 'Forester', 118),
       (472, 'Impreza', 118),
       (473, 'Legacy', 118),
       (474, 'Outback', 118),
       (475, 'Solterra', 118),
       (476, 'WRX', 118),
       (477, 'Cybertruck', 119),
       (478, 'Model 3', 119),
       (479, 'Model S', 119),
       (480, 'Model X', 119),
       (481, 'Model Y', 119),
       (482, 'Avalon', 120),
       (483, 'Avalon Hybrid', 120),
       (484, 'bZ4X', 120),
       (485, 'C-HR', 120),
       (486, 'Camry', 120),
       (487, 'Camry Hybrid', 120),
       (488, 'Corolla', 120),
       (489, 'Corolla Cross', 120),
       (490, 'Corolla Hatchback', 120),
       (491, 'Corolla Hybrid', 120),
       (492, 'GR Supra', 120),
       (493, 'GR86', 120),
       (494, 'Highlander', 120),
       (495, 'Highlander Hybrid', 120),
       (496, 'Land Cruiser', 120),
       (497, 'Mirai', 120),
       (498, 'Prius', 120),
       (499, 'Prius Prime', 120),
       (500, 'RAV4', 120),
       (501, 'RAV4 Hybrid', 120),
       (502, 'RAV4 Prime', 120),
       (503, 'Sequoia', 120),
       (504, 'Sienna', 120),
       (505, 'Tacoma Access Cab', 120),
       (506, 'Tacoma Double Cab', 120),
       (507, 'Tundra CrewMax', 120),
       (508, 'Tundra Double Cab', 120),
       (509, 'Tundra Hybrid CrewMax', 120),
       (510, 'Venza', 120),
       (511, '4Runner', 120),
       (512, '86', 120),
       (513, 'VF 8', 121),
       (514, 'VF 9', 121),
       (515, 'Arteon', 122),
       (516, 'Atlas', 122),
       (517, 'Atlas Cross Sport', 122),
       (518, 'Golf', 122),
       (519, 'Golf GTI', 122),
       (520, 'Golf R', 122),
       (521, 'ID.4', 122),
       (522, 'ID.Buzz', 122),
       (523, 'Jetta', 122),
       (524, 'Jetta GLI', 122),
       (525, 'Passat', 122),
       (526, 'Taos', 122),
       (527, 'Tiguan', 122),
       (528, 'C40 Recharge', 123),
       (529, 'S60', 123),
       (530, 'S90', 123),
       (531, 'V60', 123),
       (532, 'V90', 123),
       (533, 'XC40', 123),
       (534, 'XC40 Recharge', 123),
       (535, 'XC60', 123),
       (536, 'XC90', 123),
       (548, 'Mustang', 91),
       (549, 'DBX', 80);
/*!40000 ALTER TABLE `models`
    ENABLE KEYS */;

-- Dumping structure for table smartgarage.roles
CREATE TABLE IF NOT EXISTS `roles`
(
    `role_id` int(11)     NOT NULL AUTO_INCREMENT,
    `role`    varchar(30) NOT NULL,
    PRIMARY KEY (`role_id`),
    UNIQUE KEY `roles_role_id_uindex` (`role_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = latin1;

-- Dumping data for table smartgarage.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles`
    DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `role`)
VALUES (1, 'USER'),
       (2, 'EMPLOYEE');
/*!40000 ALTER TABLE `roles`
    ENABLE KEYS */;

-- Dumping structure for table smartgarage.services
CREATE TABLE IF NOT EXISTS `services`
(
    `service_id` int(11)      NOT NULL AUTO_INCREMENT,
    `service`    varchar(100) NOT NULL,
    `price`      double       NOT NULL,
    PRIMARY KEY (`service_id`),
    UNIQUE KEY `services_service_id_uindex` (`service_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 15
  DEFAULT CHARSET = latin1;

-- Dumping data for table smartgarage.services: ~9 rows (approximately)
/*!40000 ALTER TABLE `services`
    DISABLE KEYS */;
INSERT INTO `services` (`service_id`, `service`, `price`)
VALUES (1, 'oil change High', 325),
       (2, 'oil change Low', 120),
       (4, 'Timing belt change High', 1000),
       (5, 'Water pump change for Low', 400.5),
       (6, 'Tire change for High', 40.96),
       (8, 'Rear view mirror fix', 58),
       (9, 'A/C Repair Low', 2500),
       (10, 'A/C Repair High', 6500),
       (12, 'Timing belt change High', 750),
       (13, 'Tire change for Low', 25),
       (14, 'Paintjob whole car', 5000);
/*!40000 ALTER TABLE `services`
    ENABLE KEYS */;

-- Dumping structure for table smartgarage.users
CREATE TABLE IF NOT EXISTS `users`
(
    `user_id`      int(11)      NOT NULL AUTO_INCREMENT,
    `username`     varchar(20)  NOT NULL,
    `password`     varchar(100) NOT NULL,
    `email`        varchar(320) NOT NULL,
    `phone_number` varchar(10)  NOT NULL,
    `enabled`      tinyint(1) DEFAULT NULL,
    PRIMARY KEY (`user_id`),
    UNIQUE KEY `users_email_uindex` (`email`),
    UNIQUE KEY `users_phone_number_uindex` (`phone_number`),
    UNIQUE KEY `users_user_id_uindex` (`user_id`),
    UNIQUE KEY `users_username_uindex` (`username`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 52
  DEFAULT CHARSET = latin1;

-- Dumping data for table smartgarage.users: ~11 rows (approximately)
/*!40000 ALTER TABLE `users`
    DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `password`, `email`, `phone_number`, `enabled`)
VALUES (33, 'DiyanDimov', '$2a$10$QsYveFt1bhM/isLe4G8JGOMQst6TaLW8rx7XkYQZn7QNPinlSB25O', 'tintiri@gmail.com',
        '0896950651', 1),
       (35, 'VilislavAngelov', '$2a$10$mTOp6GEvDl6S38E0Apsg.O4xIzAfZT2tj0HLOlNUm9/d4No5AZYNe', 'muhahahahaha@gmail.com',
        '0896950652', 1),
       (36, 'Profesora', '$2a$10$rbKZVZfx.16neCRZx/C4geiodMpckGngB7nSnnrY9SThAWoDliycO', 'Dushko_dobrodushko@yahoo.com',
        '0893649558', 1),
       (37, 'Jermaine_Lebsack', '$2a$10$2WUUHAjNTU/kuG1PVrY06eh8/eS9fFIn3KEwCtIvLeoEDb4ML/pg.',
        'martinadimova84@gmail.com', '0893641557', 1),
       (38, 'Raphaelle_Skiles78', '$2a$10$BESIG.kPWUmXJB8NYfFhi.4lVD5fBNxYd1XXWKPZzRVz1amZ9rNKu',
        'Lucas_Jacobi@yahoo.com', '0893648557', 1),
       (39, 'Garnett.Muller94', '$2a$10$jSOcxWO3k/kXB1GHe0Vkz.M/kx7H55cZj4WUeSORtgI7A5stsMvd2',
        'Gabriel.Jerde57@yahoo.com', '0893641559', 1),
       (40, 'Luther_McClure', '$2a$10$J3wuE1SowOvr9V05GlQdDOvoObjqeEfziA9iwGab3i/HPtZUH4/2u',
        'Ezekiel_Okuneva46@yahoo.com', '0893641517', 1),
       (45, 'MartinaDimova', '$2a$10$yZQc6HYv1rANl/MhFkntjeTqojM1kPJf3MgvxxVSAPMIwV0ykpzAy', 'martinadimova@gmail.com',
        '0123456789', 1),
       (49, 'Konstantin', '$2a$10$LQb2PTzTevfWI.PhVShxYuNppIwADefEUGwv.DpePmeuI0fHLURA.', 'konstantin@yahoo.com',
        '0425456987', 1),
       (50, 'Konstantin123', '$2a$10$h9gM7WLwU8Y2o7KWe1YxW.nTOJEFBDUkRIh18fb99ATT01jNkt99q', 'sdgfdhx@yahoo.com',
        '0896456945', 1),
       (51, 'KirilKirilov', '$2a$10$AQCkN3jHCj4GufjUviREDu2xg8.UiKFxQaPNdlpzCSPROyqke9UtG', 'kiril@yahoo.com',
        '0896456922', 1);
/*!40000 ALTER TABLE `users`
    ENABLE KEYS */;

-- Dumping structure for table smartgarage.users_roles
CREATE TABLE IF NOT EXISTS `users_roles`
(
    `user_id` int(11) NOT NULL,
    `role_id` int(11) NOT NULL,
    KEY `users_roles_roles_role_id_fk` (`role_id`),
    KEY `users_roles_users_user_id_fk` (`user_id`),
    CONSTRAINT `users_roles_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
    CONSTRAINT `users_roles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

-- Dumping data for table smartgarage.users_roles: ~11 rows (approximately)
/*!40000 ALTER TABLE `users_roles`
    DISABLE KEYS */;
INSERT INTO `users_roles` (`user_id`, `role_id`)
VALUES (45, 2),
       (49, 1),
       (50, 2),
       (51, 2),
       (33, 2),
       (40, 2),
       (35, 1),
       (36, 1),
       (37, 1),
       (38, 1),
       (39, 1);
/*!40000 ALTER TABLE `users_roles`
    ENABLE KEYS */;

-- Dumping structure for table smartgarage.vehicles
CREATE TABLE IF NOT EXISTS `vehicles`
(
    `vehicle_id`    int(11)     NOT NULL AUTO_INCREMENT,
    `license_plate` varchar(8)  NOT NULL,
    `vin`           varchar(17) NOT NULL,
    `year`          int(11)     NOT NULL,
    `model_id`      int(11) DEFAULT NULL,
    `user_id`       int(11) DEFAULT NULL,
    PRIMARY KEY (`vehicle_id`),
    UNIQUE KEY `vehicles_vehicle_id_uindex` (`vehicle_id`),
    KEY `vehicles_models_model_id_fk` (`model_id`),
    KEY `vehicles_users_user_id_fk` (`user_id`),
    CONSTRAINT `vehicles_models_model_id_fk` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`),
    CONSTRAINT `vehicles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = latin1;

-- Dumping data for table smartgarage.vehicles: ~6 rows (approximately)
/*!40000 ALTER TABLE `vehicles`
    DISABLE KEYS */;
INSERT INTO `vehicles` (`vehicle_id`, `license_plate`, `vin`, `year`, `model_id`, `user_id`)
VALUES (1, 'EH1234KT', '12345678912345678', 2012, 109, 38),
       (2, 'B 1234KT', '12345678912345679', 2012, 2, 33),
       (5, 'B 9267KT', '6239867891254O689', 2013, 6, 35),
       (6, 'PP7267TX', '6239867091254O689', 2015, 1, 35),
       (7, 'PB9267TX', '62398A7091254O689', 2022, 54, 45),
       (8, 'CC1234TX', '96385274114725836', 1990, 548, 40),
       (9, 'TX5689TX', '96385274114725847', 2022, 549, 33);
/*!40000 ALTER TABLE `vehicles`
    ENABLE KEYS */;

-- Dumping structure for table smartgarage.visits
CREATE TABLE IF NOT EXISTS `visits`
(
    `visit_id`   int(11) NOT NULL AUTO_INCREMENT,
    `visit_date` date    NOT NULL,
    `total_cost` double  DEFAULT NULL,
    `vehicle_id` int(11) NOT NULL,
    `user_id`    int(11) DEFAULT NULL,
    PRIMARY KEY (`visit_id`),
    UNIQUE KEY `visits_visit_id_uindex` (`visit_id`),
    KEY `visits_users_user_id_fk` (`user_id`),
    KEY `visits_vehicles_vehicle_id_fk` (`vehicle_id`),
    CONSTRAINT `visits_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
    CONSTRAINT `visits_vehicles_vehicle_id_fk` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`vehicle_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 16
  DEFAULT CHARSET = latin1;

-- Dumping data for table smartgarage.visits: ~5 rows (approximately)
/*!40000 ALTER TABLE `visits`
    DISABLE KEYS */;
INSERT INTO `visits` (`visit_id`, `visit_date`, `total_cost`, `vehicle_id`, `user_id`)
VALUES (9, '2022-01-12', 6825, 7, 45),
       (10, '2022-02-16', 1120, 6, 35),
       (11, '2022-03-16', 400.5, 5, 35),
       (12, '2022-04-06', 725.5, 1, 38),
       (13, '2022-04-03', 2558, 2, 33),
       (14, '2022-02-02', 775, 1, 38),
       (15, '2022-03-11', 445, 2, 33);
/*!40000 ALTER TABLE `visits`
    ENABLE KEYS */;

-- Dumping structure for table smartgarage.visits_h_services
CREATE TABLE IF NOT EXISTS `visits_h_services`
(
    `visit_h_service_id` int(11) NOT NULL AUTO_INCREMENT,
    `visit_id`           int(11) NOT NULL,
    `h_service_id`       int(11) NOT NULL,
    PRIMARY KEY (`visit_h_service_id`),
    UNIQUE KEY `visits_h_services_visit_h_service_id_uindex` (`visit_h_service_id`),
    KEY `visits_h_services_visits_visit_id_fk` (`visit_id`),
    KEY `visits_h_services_h_services_h_service_id_fk` (`h_service_id`),
    CONSTRAINT `visits_h_services_h_services_h_service_id_fk` FOREIGN KEY (`h_service_id`) REFERENCES `h_services` (`h_service_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `visits_h_services_visits_visit_id_fk` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`visit_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  AUTO_INCREMENT = 25
  DEFAULT CHARSET = latin1;

-- Dumping data for table smartgarage.visits_h_services: ~11 rows (approximately)
/*!40000 ALTER TABLE `visits_h_services`
    DISABLE KEYS */;
INSERT INTO `visits_h_services` (`visit_h_service_id`, `visit_id`, `h_service_id`)
VALUES (4, 9, 10),
       (7, 13, 8),
       (15, 12, 5),
       (16, 11, 5),
       (17, 15, 1),
       (18, 9, 1),
       (19, 12, 1),
       (21, 10, 2),
       (22, 15, 2),
       (23, 10, 4),
       (24, 13, 9);
/*!40000 ALTER TABLE `visits_h_services`
    ENABLE KEYS */;

-- Dumping structure for table smartgarage.visits_services
CREATE TABLE IF NOT EXISTS `visits_services`
(
    `visit_service_id` int(11) NOT NULL AUTO_INCREMENT,
    `visit_id`         int(11) DEFAULT NULL,
    `service_id`       int(11) DEFAULT NULL,
    PRIMARY KEY (`visit_service_id`),
    UNIQUE KEY `visits_services_visit_service_id_uindex` (`visit_service_id`),
    KEY `visits_services_services_service_id_fk` (`service_id`),
    KEY `visits_services_visits_visit_id_fk` (`visit_id`),
    CONSTRAINT `visits_services_services_service_id_fk` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`),
    CONSTRAINT `visits_services_visits_visit_id_fk` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`visit_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 81
  DEFAULT CHARSET = latin1;

-- Dumping data for table smartgarage.visits_services: ~11 rows (approximately)
/*!40000 ALTER TABLE `visits_services`
    DISABLE KEYS */;
INSERT INTO `visits_services` (`visit_service_id`, `visit_id`, `service_id`)
VALUES (41, 9, 10),
       (57, 13, 8),
       (61, 12, 5),
       (63, 11, 5),
       (64, 15, 1),
       (65, 9, 1),
       (66, 12, 1),
       (73, 10, 2),
       (74, 15, 2),
       (77, 10, 4),
       (80, 13, 9);
/*!40000 ALTER TABLE `visits_services`
    ENABLE KEYS */;

/*!40101 SET SQL_MODE = IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS = IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES = IFNULL(@OLD_SQL_NOTES, 1) */;
