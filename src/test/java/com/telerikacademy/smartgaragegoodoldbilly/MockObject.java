package com.telerikacademy.smartgaragegoodoldbilly;

import com.telerikacademy.smartgaragegoodoldbilly.models.Role;
import com.telerikacademy.smartgaragegoodoldbilly.models.User;
import com.telerikacademy.smartgaragegoodoldbilly.models.dto.UserDto;


public class MockObject {

    public static User createMockUser() {
        Role role = new Role(1, "USER");
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword");
        mockUser.setEmail("mock@user.com");
        mockUser.setPhone_number("1111111111");
        mockUser.addUserRole(role);
        return mockUser;
    }

    public static User createMockEmployee() {
        Role role = new Role(1, "EMPLOYEE");
        var mockEmployee = new User();
        mockEmployee.setId(2);
        mockEmployee.setUsername("MockEmployee");
        mockEmployee.setPassword("MockPassword");
        mockEmployee.setEmail("employee@user.com");
        mockEmployee.setPhone_number("2222222222");
        mockEmployee.addUserRole(role);
        return mockEmployee;
    }

    public static UserDto createMockUserDTO() {
        var mockUserDTO = new UserDto();
        mockUserDTO.setUsername("MockUsername");
        mockUserDTO.setPassword("MockPassword");
        mockUserDTO.setEmail("mock@user.com");
        mockUserDTO.setPhone_number("1111111111");
        return mockUserDTO;
    }
}
